#!/bin/sh

#======
ready() 
#======
{

if test ! -z "$SCM_RESULTDIR" -a ! -z "$JOBNAME"
then

#  special case to make sure results from chained jobs are copied back, sub jobs have path in JOBNAME
   AAA=`dirname "$JOBNAME"`
   JOBNAME=`basename "$JOBNAME"`
   if [ "$AAA" != "." ]; then
      SCM_RESULTDIR=$SCM_RESULTDIR/$AAA
   fi

    # -------------------------------------------
    # copy any files produced to result directory
    # -------------------------------------------

    for n in 13 21 41 10 11 15
    do
        cp -f -p "TAPE$n" "$SCM_RESULTDIR/$JOBNAME.t$n" 2>/dev/null
        rm -f "TAPE$n"
    done

    cp -f -p RUNKF "$SCM_RESULTDIR/$JOBNAME.runkf" 2>/dev/null
    cp -f -p COSKF "$SCM_RESULTDIR/$JOBNAME.coskf" 2>/dev/null
    cp -f -p CRSKF "$SCM_RESULTDIR/$JOBNAME.crskf" 2>/dev/null
    cp -f -p QUILDKF "$SCM_RESULTDIR/$JOBNAME.qkf" 2>/dev/null
    cp -f -p quildjob.qkf "$SCM_RESULTDIR/$JOBNAME.qkf" 2>/dev/null
    rm -f  QUILDKF quildjob.qkf COSKF CRSKF RUNKF

    if test ! -d "$SCM_RESULTDIR/$JOBNAME.results"
    then
        mkdir -p "$SCM_RESULTDIR/$JOBNAME.results"
    fi
    if test -d "$SCM_RESULTDIR/$JOBNAME.results"
    then
        for n in 13 21 41 10 11 15
        do
            cp -f -p "TAPE$n" "$SCM_RESULTDIR/$JOBNAME.t$n" 2>/dev/null
            rm -f "TAPE$n"
        done
        touch thisfileshouldbeignored
        cp -rf -p * "$SCM_RESULTDIR/$JOBNAME.results"
    else
        echo Problem copying result files to \"$SCM_RESULTDIR/$JOBNAME.results\"
    fi

    for f in "$SCM_RESULTDIR/$JOBNAME.results"/* thisfiledoesnotexist
    do
        if test -L "$f"; then
            rm -f "$f"
        fi
    done
    rm -f "$SCM_RESULTDIR/$JOBNAME.results"/thisfileshouldbeignored

    if test -z "$1"
    then
        echo "Job $JOBNAME has finished" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    else
        echo "Job $JOBNAME: $1" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    fi
    cd "$OWD" 2>/dev/null
    rm -rf "$SCM_JOBDIR"
    if test -z "$SCM_NOREADY"; then
        echo "# status: ready" >> "$SCM_RESULTDIR/$JOBNAME.pid"
#       densf runs from AMSview remove the line above, for that reason an extra NOP line is needed
        DUMMY=blurb
    fi
fi
}


#-----------------------------------------
# keep track of original working directory
#-----------------------------------------

OWD="`pwd`"
export OWD

# -------------------------------------------------
# insert prolog command as defined in AMSjobs queue
# -------------------------------------------------

# <<PROLOG>>

# ---------------------------------------------------
# execute arguments (possibly to set environment etc)
# ---------------------------------------------------

if test ! -z "$1"
then
    if test "$1" != "export NSCM=" -a "$1" != "NSCM= ; export NSCM"; then
       eval $1
    fi
fi

# ---------------
# name of the job
# ---------------

JOBNAME=`basename "$0" .job`
export JOBNAME

# -----------------------------------------------------------------------------------
# on a batch system, the start.cmd will replace the next line to set JOBNAME properly
# since the batch system may make a copy of the job script
# -----------------------------------------------------------------------------------

# <<JOBNAME>>

# -------------------------------------------------
# determine DIRN: full path of the script directory
# -------------------------------------------------

DN="`dirname "$0"`"
case "$DN" in
/*)
DIRN="$DN"
;;
?:*)
DIRN="$DN"
;;
*)
DIRN="`pwd`/$DN"
;;
esac

# --------------------------------------------------------------------------------
# on a batch system, the start.cmd will replace the next line to set DIRN properly
# since the batch system may make a copy of the job script
# --------------------------------------------------------------------------------

# <<FULL PATH OF SCRIPT DIRECTORY DIRN>>

# ------------------------------------------------------------------------------------
# set SCM_RESULTDIR to DIRN, so results end up in the full path as required by ADFJOBS
# ------------------------------------------------------------------------------------

SCM_RESULTDIR="$DIRN"
export SCM_RESULTDIR

#----------------------------
# for AMS, set AMS_RESULTSDIR
#----------------------------

AMS_RESULTSDIR="$SCM_RESULTDIR/$JOBNAME.results"
export AMS_RESULTSDIR
rm -rf "$AMS_RESULTSDIR"
mkdir -p "$AMS_RESULTSDIR"
unset AMS_SWITCH_LOGFILE_AND_STDOUT

# ----------------------------------
# pass the location of the info file
# ----------------------------------

SCM_INFOFILE="$SCM_RESULTDIR/$JOBNAME.info"
export SCM_INFOFILE

# ---------------------------------
# update the status in the pid file
# ---------------------------------

echo "# pid: $$" >> "$SCM_RESULTDIR/$JOBNAME.pid"
echo "# status: running" >> "$SCM_RESULTDIR/$JOBNAME.pid"


# ---------------
# prepare logfile
# ---------------

if test -s "$SCM_RESULTDIR/$JOBNAME.logfile"
then
    cp "$SCM_RESULTDIR/$JOBNAME.logfile" "$SCM_RESULTDIR/$JOBNAME.logfile~"
fi
cat /dev/null > "$SCM_RESULTDIR/$JOBNAME.logfile"

SCM_LOGFILE="$SCM_RESULTDIR/$JOBNAME.logfile"
export SCM_LOGFILE


# <<FRAGMENT JOB SCRIPTS>>


# ------------------------------------------------------------------
# within SCM_RESULTDIR, make a new empty directory just for this job
# ------------------------------------------------------------------

XX=0
SCM_JOBDIR="$SCM_RESULTDIR/tmp.$USER.$$.$XX.noindex"
while test -d "$SCM_JOBDIR" 
do
   XX=`expr $XX + 1`
   SCM_JOBDIR="$SCM_RESULTDIR/tmp.$USER.$$.$XX.noindex"
done
export SCM_JOBDIR
mkdir "$SCM_JOBDIR"
echo "# jobdir: localhost:$SCM_JOBDIR" >> "$SCM_INFOFILE"

if test ! -d "$SCM_JOBDIR"
then
    echo "Could not create temporary directory $SCM_JOBDIR" >> "$SCM_RESULTDIR/$JOBNAME.logfile"
    echo "# status: killed" >> "$SCM_RESULTDIR/$JOBNAME.pid"
    exit
fi

# -----------------------------------------------------
# nodeinfo may be needed, and keep backup of old output
# -----------------------------------------------------

if test -f nodeinfo 
then
    cp nodeinfo "$SCM_JOBDIR/nodeinfo"
fi
cd "$SCM_JOBDIR"

if test -s "$SCM_RESULTDIR/$JOBNAME.out"
then
    mv "$SCM_RESULTDIR/$JOBNAME.out" "$SCM_RESULTDIR/$JOBNAME.out~"
fi

# <<COPY FRAGMENT FILES>>

# -------------------------------------------------------
# on normal or abnormal exits, clean up and update status
# -------------------------------------------------------

trap "ready \"abnormal exit (signal 1)\"; exit"  1 
trap "ready \"abnormal exit (signal 2)\"; exit"  2 
trap "ready \"abnormal exit (signal 3)\"; exit"  3
trap "ready \"abnormal exit (signal 15)\"; exit" 15

# -------------------------------------
# define easy vars for output and error
# -------------------------------------

SCM_OUT="$SCM_RESULTDIR/$JOBNAME.out"
export SCM_OUT
SCM_ERR="$SCM_RESULTDIR/$JOBNAME.err"
export SCM_ERR


# ----------------------
# execute the run script
# ----------------------

cat << \EORTHEJOB > job
#!/bin/sh

"$AMSBIN/ams" << eor

Task GeometryOptimization
UseSymmetry No
Properties
    NormalModes Yes
End
NormalModes
    Hessian Numerical
End
System
    Atoms
        C 0.0534710050546 0.07538921311464999 -0.10872781662368 adf.R=1.700
        C 1.05283094875853 1.03659962919993 -0.24719215701752 adf.R=1.700
        C 0.3469568675113 -1.17117907536766 0.46153571553193 adf.R=1.700
        C 2.36173803648983 0.75276759584424 0.18661762119366 adf.R=1.700
        C 2.65676525883432 -0.49708923983696 0.75598846047018 adf.R=1.700
        C 1.64794644356405 -1.45338849761809 0.89211577934293 adf.R=1.700
        H 0.83716365809592 2.00244684327013 -0.68882009111592 adf.R=1.350
        C 3.40782331228307 1.76171295541231 0.04403537760241 adf.R=1.700
        H -0.95353508932148 0.29633272112756 -0.44570968704654 adf.R=1.350
        H -0.43442150945607 -1.91640189254403 0.56789982284081 adf.R=1.350
        H 3.66688394198091 -0.70786805217792 1.08447956879312 adf.R=1.350
        H 1.87839802092937 -2.41719787211676 1.33307309771797 adf.R=1.350
        N 3.21707703597448 2.93359228085407 -0.4795892026497 adf.R=1.608
        O 4.37058989934538 3.74360995147806 -0.4927987428999 adf.R=1.517
        O 4.67743767829713 1.41953402965198 0.51789781509984 adf.R=1.517
        S 5.93080379595339 2.71231964649604 0.01473946571287 adf.R=1.792
        O 6.65647069570523 2.22581976321243 -1.32484502695244 adf.R=1.517
    End
    BondOrders
         1 2 1.5
         1 3 1.5
         1 9 1.0
         2 4 1.5
         2 7 1.0
         3 6 1.5
         3 10 1.0
         4 5 1.5
         4 8 1.0
         5 6 1.5
         5 11 1.0
         6 12 1.0
         8 13 2.0
         8 15 1.0
         13 14 1.0
         14 16 1.0
         15 16 1.0
         16 17 2.0
    End
End

Engine ADF
    Basis
        Type TZ2P
        Core None
    End
    XC
        LibXC r2SCAN
        DISPERSION GRIMME4
    End
    Solvation
        Surf Delley
        Solv name=Benzene cav0=0.0 cav1=0.0067639
        Charged method=CONJ
        C-Mat POT
        SCF VAR ALL 
        CSMRSP
    End
    NumericalQuality Good
EndEngine
eor



EORTHEJOB

chmod u+x job
./job >>"$SCM_RESULTDIR/$JOBNAME.out" 2>> "$SCM_RESULTDIR/$JOBNAME.err"
rm job

# <<EPILOG>>

ready

