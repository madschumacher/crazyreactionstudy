-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1152.6883908687
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 57.9999665670 
   Number of Beta  Electrons                 57.9999665670 
   Total number of  Electrons               115.9999331340 
   Exchange energy                         -117.5003622259 
   Correlation energy                        -4.0108406297 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5112028557 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6883908687 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1065
     Surface Area:         812.6734644474
     Dielectric Energy:     -0.0081442513
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0082615947
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1152.6884579518
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 57.9999660064 
   Number of Beta  Electrons                 57.9999660064 
   Total number of  Electrons               115.9999320128 
   Exchange energy                         -117.4981457781 
   Correlation energy                        -4.0106551088 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5088008869 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6884579518 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1065
     Surface Area:         813.0473531684
     Dielectric Energy:     -0.0082316184
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0082605947
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1152.6884056387
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 57.9999665314 
   Number of Beta  Electrons                 57.9999665314 
   Total number of  Electrons               115.9999330629 
   Exchange energy                         -117.4966997797 
   Correlation energy                        -4.0105464045 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5072461842 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6884056387 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1066
     Surface Area:         813.0036033048
     Dielectric Energy:     -0.0083150357
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0082613154
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1152.6884943794
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 57.9999657087 
   Number of Beta  Electrons                 57.9999657087 
   Total number of  Electrons               115.9999314175 
   Exchange energy                         -117.4970931080 
   Correlation energy                        -4.0105875209 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5076806289 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6884943794 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1067
     Surface Area:         813.0929367207
     Dielectric Energy:     -0.0083192072
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0082623172
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1152.6884887866
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 57.9999659275 
   Number of Beta  Electrons                 57.9999659275 
   Total number of  Electrons               115.9999318550 
   Exchange energy                         -117.4977825413 
   Correlation energy                        -4.0106370574 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5084195988 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6884887866 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1066
     Surface Area:         812.9707997332
     Dielectric Energy:     -0.0082840919
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0082622785
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1152.6884917589
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 57.9999659097 
   Number of Beta  Electrons                 57.9999659097 
   Total number of  Electrons               115.9999318194 
   Exchange energy                         -117.4976799018 
   Correlation energy                        -4.0106284453 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5083083471 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6884917589 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1066
     Surface Area:         812.9688039276
     Dielectric Energy:     -0.0082938158
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0082623823
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 6
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.8189248953
        Electronic Contribution:
                  0    
      0       4.552649
      1       6.043055
      2      -1.102050
        Nuclear Contribution:
                  0    
      0      -3.248490
      1      -7.149833
      2       2.623589
        Total Dipole moment:
                  0    
      0       1.304159
      1      -1.106778
      2       1.521539
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1152.6884917593
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 57.9999659099 
   Number of Beta  Electrons                 57.9999659099 
   Total number of  Electrons               115.9999318198 
   Exchange energy                         -117.4976874687 
   Correlation energy                        -4.0106286785 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.5083161472 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6884917593 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1066
     Surface Area:         812.9688039276
     Dielectric Energy:     -0.0082937546
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0082623823
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        227.9840924101
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1152.6713526363
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0095725416
        Number of frequencies          :     57      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      19.790985
      7      41.965559
      8      75.926646
      9     100.117500
     10     143.319624
     11     173.255860
     12     227.232218
     13     271.614445
     14     289.046569
     15     303.876457
     16     392.028287
     17     418.775613
     18     424.637718
     19     434.855065
     20     471.345963
     21     498.347177
     22     549.766709
     23     631.636236
     24     652.518136
     25     670.905491
     26     674.864967
     27     687.775735
     28     755.277095
     29     756.788753
     30     820.624162
     31     880.588178
     32     901.236768
     33     925.879672
     34     947.127686
     35     948.713179
     36     999.226451
     37     1022.979223
     38     1092.817170
     39     1115.094273
     40     1117.329152
     41     1194.290240
     42     1219.330747
     43     1304.815312
     44     1329.970794
     45     1355.619682
     46     1372.041215
     47     1465.436422
     48     1505.481798
     49     1566.645545
     50     1601.320063
     51     1630.851078
     52     1649.551311
     53     3192.502334
     54     3206.973680
     55     3221.918113
     56     3223.663308
        Zero Point Energy (Hartree)    :          0.1149966539
        Inner Energy (Hartree)         :      -1152.5439508980
        Enthalpy (Hartree)             :      -1152.5430066890
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0154930088
        Vibrational entropy            :          0.0168454331
        Translational entropy          :          0.0154930088
        Entropy                        :          0.0523768414
        Gibbs Energy (Hartree)         :      -1152.5953835303
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.167418113736    0.053892636160   -0.054979901666
               1 C      1.124776614667    1.038798297167   -0.210946808843
               2 C      0.450425580582   -1.192338070957    0.494174934474
               3 C      2.432259986454    0.759956557336    0.199610611107
               4 C      2.742711410123   -0.485745885859    0.755692121787
               5 C      1.752423927924   -1.451372763830    0.899722138707
               6 H      0.861069439728    1.997700921568   -0.640938404623
               7 C      3.467927009849    1.778332700851    0.048032693430
               8 N     -1.220561273622    0.342390075034   -0.489302956431
               9 H     -0.338923357585   -1.926859851081    0.594565969218
              10 H      3.759287830089   -0.695733008057    1.069554940323
              11 H      2.000479196882   -2.415874736194    1.330575524214
              12 N      3.297100427027    2.890082407000   -0.569977203078
              13 O      4.474544013721    3.640852202763   -0.509464344799
              14 O      4.692004059768    1.525773002388    0.605233763108
              15 S      5.833804871074    2.678059210875   -0.004016875473
              16 O      6.464416347154    2.088285289986   -1.185794972198
              17 O     -1.444892256667    1.444416604151   -0.968114229366
              18 O     -2.050191940905   -0.544205589302   -0.338826999892
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.166952234236    0.053698162270   -0.054246967240
               1 C      1.123692426173    1.039644687197   -0.210330737735
               2 C      0.450763143844   -1.192919502707    0.495017591238
               3 C      2.431258978159    0.760737370822    0.199035378722
               4 C      2.742684918205   -0.485168293933    0.755069752082
               5 C      1.752890817465   -1.451277827435    0.900042685296
               6 H      0.859690200817    1.998941325729   -0.638970719367
               7 C      3.467187423777    1.779176326319    0.046736643237
               8 N     -1.219571339644    0.342063514306   -0.488467317008
               9 H     -0.336991655480   -1.929651521832    0.596859928971
              10 H      3.759306895025   -0.695771878548    1.068767224773
              11 H      2.001434431835   -2.415544954239    1.331024811461
              12 N      3.298771861759    2.891235351766   -0.572030121251
              13 O      4.478440745742    3.641584548496   -0.508814816032
              14 O      4.690785631785    1.526110534328    0.603126191108
              15 S      5.835607655864    2.677239014082   -0.000847970408
              16 O      6.461451360011    2.086260796336   -1.185837204529
              17 O     -1.446806205179    1.443916602004   -0.967974849741
              18 O     -2.051469524394   -0.543864254963   -0.343359503575
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     3 
    Coordinates:
               0 C      0.167458279012    0.051754604559   -0.058883916321
               1 C      1.123397712260    1.039080383293   -0.214575634736
               2 C      0.451543853177   -1.194180254039    0.492642350888
               3 C      2.430379866350    0.761101917003    0.196233383052
               4 C      2.742538821561   -0.484033035433    0.754243911570
               5 C      1.753301841213   -1.450789575401    0.899703077291
               6 H      0.859506008438    1.998235108136   -0.643441117135
               7 C      3.466280958727    1.779780299373    0.043784601589
               8 N     -1.215900923222    0.338099419727   -0.495675855758
               9 H     -0.333714007261   -1.933745975017    0.595827069938
              10 H      3.758585102368   -0.694837111540    1.069787400201
              11 H      2.001904856934   -2.414156483196    1.332544303385
              12 N      3.300428199698    2.891344994157   -0.577537724724
              13 O      4.481781567753    3.642080218400   -0.510638004996
              14 O      4.687622418793    1.527360130521    0.603219785738
              15 S      5.835047490323    2.677159161875    0.005741743616
              16 O      6.461300219372    2.083843211493   -1.179453633215
              17 O     -1.451214276687    1.448205016964   -0.954167791129
              18 O     -2.054167988809   -0.539892030875   -0.334553949257
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     4 
    Coordinates:
               0 C      0.166432624476    0.053855529782   -0.054222098295
               1 C      1.122745883404    1.040744382809   -0.210861203344
               2 C      0.450821340109   -1.192756929521    0.495264362711
               3 C      2.430239371257    0.761569969257    0.197392831498
               4 C      2.742819945956   -0.484220507149    0.753502515561
               5 C      1.753219123403   -1.450503202688    0.899759641330
               6 H      0.858691107329    2.000588120439   -0.638152438402
               7 C      3.466377624015    1.779775655266    0.044178348889
               8 N     -1.217019002709    0.341276761274   -0.487338022087
               9 H     -0.334126857104   -1.932288323312    0.599417578861
              10 H      3.759192878288   -0.696248099471    1.066988578885
              11 H      2.001971697673   -2.414441593323    1.331241127694
              12 N      3.299788543925    2.892032554138   -0.575892654659
              13 O      4.481656667595    3.642151168600   -0.510339267862
              14 O      4.687975818991    1.526070222268    0.602128868478
              15 S      5.834398822404    2.675675638349    0.003996212666
              16 O      6.459804590034    2.083102652342   -1.182237337624
              17 O     -1.448378869348    1.444922624399   -0.963620621047
              18 O     -2.050531309697   -0.544896623459   -0.346406423252
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     5 
    Coordinates:
               0 C      0.166536014624    0.053878028199   -0.054671216071
               1 C      1.123237681754    1.040370639464   -0.210859699019
               2 C      0.450864483882   -1.192606584781    0.494771095736
               3 C      2.430702488673    0.761427127297    0.198107759932
               4 C      2.742917150450   -0.484284274715    0.754228837846
               5 C      1.753184513690   -1.450474093706    0.899743065575
               6 H      0.859368456910    1.999973981759   -0.638779450024
               7 C      3.466704748099    1.779848734643    0.045372506037
               8 N     -1.217736407952    0.341363339265   -0.488287351691
               9 H     -0.334535238523   -1.931568826643    0.598159448370
              10 H      3.759237225284   -0.696060279099    1.067990970779
              11 H      2.001758204179   -2.414548769293    1.331083279068
              12 N      3.299360063219    2.891446955583   -0.575018933958
              13 O      4.480327863307    3.641788504678   -0.509631579419
              14 O      4.688804627830    1.526964228818    0.603765046294
              15 S      5.834417376493    2.675770622982    0.001859931162
              16 O      6.459323164310    2.082857064705   -1.183873888600
              17 O     -1.448034154552    1.445351021858   -0.963368737041
              18 O     -2.050358261676   -0.545087421014   -0.345791084977
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     6 
    Coordinates:
               0 C      0.166380174544    0.053901487198   -0.054644183225
               1 C      1.123092489555    1.040462524241   -0.210875543326
               2 C      0.450859340774   -1.192583750037    0.494781458040
               3 C      2.430577038176    0.761483309323    0.197832114295
               4 C      2.742963369301   -0.484210745726    0.753941819450
               5 C      1.753231773113   -1.450390612275    0.899637696564
               6 H      0.859218328605    2.000151504107   -0.638605486423
               7 C      3.466583643173    1.779906594400    0.044907963017
               8 N     -1.217577183413    0.341321438359   -0.488061762456
               9 H     -0.334107808322   -1.931987451598    0.598526059864
              10 H      3.759255878689   -0.696222827639    1.067641044997
              11 H      2.001810178911   -2.414443245449    1.331010056540
              12 N      3.299508395935    2.891342397359   -0.575951758533
              13 O      4.480736515639    3.641743124692   -0.510437413821
              14 O      4.688417994640    1.527214024267    0.603785824965
              15 S      5.834148270173    2.675822261434    0.002628682147
              16 O      6.459707618151    2.082467424345   -1.182729987042
              17 O     -1.448437266465    1.445706617847   -0.962221232725
              18 O     -2.050288751180   -0.545274074847   -0.346365352326
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     7 
    Coordinates:
               0 C      0.166380174544    0.053901487198   -0.054644183225
               1 C      1.123092489555    1.040462524241   -0.210875543326
               2 C      0.450859340774   -1.192583750037    0.494781458040
               3 C      2.430577038176    0.761483309323    0.197832114295
               4 C      2.742963369301   -0.484210745726    0.753941819450
               5 C      1.753231773113   -1.450390612275    0.899637696564
               6 H      0.859218328605    2.000151504107   -0.638605486423
               7 C      3.466583643173    1.779906594400    0.044907963017
               8 N     -1.217577183413    0.341321438359   -0.488061762456
               9 H     -0.334107808322   -1.931987451598    0.598526059864
              10 H      3.759255878689   -0.696222827639    1.067641044997
              11 H      2.001810178911   -2.414443245449    1.331010056540
              12 N      3.299508395935    2.891342397359   -0.575951758533
              13 O      4.480736515639    3.641743124692   -0.510437413821
              14 O      4.688417994640    1.527214024267    0.603785824965
              15 S      5.834148270173    2.675822261434    0.002628682147
              16 O      6.459707618151    2.082467424345   -1.182729987042
              17 O     -1.448437266465    1.445706617847   -0.962221232725
              18 O     -2.050288751180   -0.545274074847   -0.346365352326
