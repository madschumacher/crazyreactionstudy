-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1027106762
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.9455     6.0000     0.0545     3.7843     3.7843     0.0000
  1   0     6.1370     6.0000    -0.1370     3.9034     3.9034    -0.0000
  2   0     6.1402     6.0000    -0.1402     3.8793     3.8793     0.0000
  3   0     5.9286     6.0000     0.0714     3.7514     3.7514     0.0000
  4   0     6.1187     6.0000    -0.1187     3.8375     3.8375     0.0000
  5   0     6.1314     6.0000    -0.1314     3.7491     3.7491     0.0000
  6   0     0.7854     1.0000     0.2146     0.9816     0.9816     0.0000
  7   0     5.8279     6.0000     0.1721     3.8243     3.8243     0.0000
  8   0     6.5119     7.0000     0.4881     4.2338     4.2338    -0.0000
  9   0     0.7897     1.0000     0.2103     0.9766     0.9766    -0.0000
 10   0     0.8083     1.0000     0.1917     0.9935     0.9935     0.0000
 11   0     0.8256     1.0000     0.1744     0.9727     0.9727     0.0000
 12   0     6.8145     7.0000     0.1855     3.6458     3.6458     0.0000
 13   0     8.4460     8.0000    -0.4460     1.7965     1.7965    -0.0000
 14   0     8.5511     8.0000    -0.5511     1.9406     1.9406     0.0000
 15   0    15.0572    16.0000     0.9428     3.8640     3.8640    -0.0000
 16   0     8.4803     8.0000    -0.4803     2.0294     2.0294    -0.0000
 17   0     8.3494     8.0000    -0.3494     1.9034     1.9034    -0.0000
 18   0     8.3513     8.0000    -0.3513     1.9002     1.9002    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.399049
                0             6               2            6                1.372132
                0             6               8            7                0.943153
                1             6               3            6                1.296267
                1             6               6            1                0.943209
                2             6               5            6                1.362659
                2             6               9            1                0.943206
                3             6               4            6                1.341070
                3             6               7            6                1.071749
                4             6               5            6                1.371758
                4             6              10            1                0.950756
                5             6              11            1                0.971046
                7             6              12            7                2.288266
                7             6              13            8                0.183968
                7             6              14            8                0.227812
                8             7              17            8                1.650243
                8             7              18            8                1.647124
               12             7              13            8                1.241804
               13             8              15           16                0.293100
               14             8              15           16                1.606969
               15            16              16            8                1.904884
               17             8              18            8                0.164481
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1027106731
        Total Correlation Energy:                                          -3.0173062973
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1546252894
        Total MDCI Energy:                                              -1151.1200169703
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.1664306963
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0151     6.0000    -0.0151     3.7863     3.7863     0.0000
  1   0     6.0943     6.0000    -0.0943     3.8887     3.8887    -0.0000
  2   0     6.1048     6.0000    -0.1048     3.8444     3.8444     0.0000
  3   0     6.0176     6.0000    -0.0176     3.7707     3.7707     0.0000
  4   0     6.1283     6.0000    -0.1283     3.8101     3.8101    -0.0000
  5   0     6.0854     6.0000    -0.0854     3.7366     3.7366     0.0000
  6   0     0.7714     1.0000     0.2286     0.9848     0.9848    -0.0000
  7   0     5.6750     6.0000     0.3250     3.7590     3.7590     0.0000
  8   0     6.5469     7.0000     0.4531     4.2899     4.2899     0.0000
  9   0     0.7743     1.0000     0.2257     0.9772     0.9772    -0.0000
 10   0     0.8107     1.0000     0.1893     1.0066     1.0066    -0.0000
 11   0     0.8309     1.0000     0.1691     0.9794     0.9794     0.0000
 12   0     6.8984     7.0000     0.1016     3.5992     3.5992     0.0000
 13   0     8.4325     8.0000    -0.4325     1.8264     1.8264     0.0000
 14   0     8.5954     8.0000    -0.5954     1.8964     1.8964     0.0000
 15   0    14.9760    16.0000     1.0240     3.8547     3.8547     0.0000
 16   0     8.5378     8.0000    -0.5378     1.9723     1.9723    -0.0000
 17   0     8.3516     8.0000    -0.3516     1.9104     1.9104    -0.0000
 18   0     8.3534     8.0000    -0.3534     1.9098     1.9098    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.395956
                0             6               2            6                1.358133
                0             6               8            7                0.986055
                1             6               3            6                1.288419
                1             6               6            1                0.946325
                2             6               5            6                1.375828
                2             6               9            1                0.942637
                3             6               4            6                1.306881
                3             6               7            6                1.142372
                4             6               5            6                1.386249
                4             6              10            1                0.958204
                5             6              11            1                0.981097
                7             6              12            7                2.206947
                7             6              13            8                0.160923
                7             6              14            8                0.242784
                8             7              17            8                1.674155
                8             7              18            8                1.673912
               12             7              13            8                1.263136
               13             8              15           16                0.317682
               14             8              15           16                1.594891
               15            16              16            8                1.879740
               17             8              18            8                0.159517
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1664306873
        Total Correlation Energy:                                          -3.2060848365
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1687853790
        Total MDCI Energy:                                              -1151.3725155238
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1027106731 
       SCF energy with basis  cc-pVQZ :  -1148.1664306873 
       CBS SCF Energy                    :  -1148.1856297776 
       Correlation energy with basis  cc-pVTZ :  -1148.1027106731 
       Correlation energy with basis  cc-pVQZ :  -1148.1664306873 
       CBS Correlation Energy            :     -3.3404742933 
       CBS Total Energy                  :  -1151.5261040709 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14536-MopS/ts1.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.4794614069
        Electronic Contribution:
                  0    
      0       3.616939
      1       6.008441
      2      -0.374494
        Nuclear Contribution:
                  0    
      0      -3.316912
      1      -7.188104
      2       1.648916
        Total Dipole moment:
                  0    
      0       0.300027
      1      -1.179663
      2       1.274421
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.313780000000   -0.829326000000   -0.387420000000
               1 C     -1.482327000000    0.268330000000   -0.519795000000
               2 C     -1.893313000000   -2.028362000000    0.175545000000
               3 C     -0.158499000000    0.148044000000   -0.073931000000
               4 C      0.289952000000   -1.050437000000    0.501972000000
               5 C     -0.581025000000   -2.124260000000    0.623327000000
               6 H     -1.852369000000    1.186497000000   -0.959518000000
               7 C      0.729841000000    1.250912000000   -0.201734000000
               8 N     -3.707900000000   -0.717730000000   -0.876726000000
               9 H     -2.588829000000   -2.854542000000    0.259462000000
              10 H      1.314516000000   -1.115667000000    0.849086000000
              11 H     -0.231175000000   -3.049437000000    1.069327000000
              12 N      0.990741000000    2.299687000000   -0.730449000000
              13 O      1.856704000000    3.189006000000   -0.711720000000
              14 O      2.298462000000    1.164909000000    1.080613000000
              15 S      3.415038000000    2.102060000000    0.631684000000
              16 O      4.320714000000    1.527258000000   -0.378581000000
              17 O     -4.040446000000    0.340544000000   -1.390843000000
              18 O     -4.429356000000   -1.694867000000   -0.737889000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.313780000000   -0.829326000000   -0.387420000000
               1 C     -1.482327000000    0.268330000000   -0.519795000000
               2 C     -1.893313000000   -2.028362000000    0.175545000000
               3 C     -0.158499000000    0.148044000000   -0.073931000000
               4 C      0.289952000000   -1.050437000000    0.501972000000
               5 C     -0.581025000000   -2.124260000000    0.623327000000
               6 H     -1.852369000000    1.186497000000   -0.959518000000
               7 C      0.729841000000    1.250912000000   -0.201734000000
               8 N     -3.707900000000   -0.717730000000   -0.876726000000
               9 H     -2.588829000000   -2.854542000000    0.259462000000
              10 H      1.314516000000   -1.115667000000    0.849086000000
              11 H     -0.231175000000   -3.049437000000    1.069327000000
              12 N      0.990741000000    2.299687000000   -0.730449000000
              13 O      1.856704000000    3.189006000000   -0.711720000000
              14 O      2.298462000000    1.164909000000    1.080613000000
              15 S      3.415038000000    2.102060000000    0.631684000000
              16 O      4.320714000000    1.527258000000   -0.378581000000
              17 O     -4.040446000000    0.340544000000   -1.390843000000
              18 O     -4.429356000000   -1.694867000000   -0.737889000000
