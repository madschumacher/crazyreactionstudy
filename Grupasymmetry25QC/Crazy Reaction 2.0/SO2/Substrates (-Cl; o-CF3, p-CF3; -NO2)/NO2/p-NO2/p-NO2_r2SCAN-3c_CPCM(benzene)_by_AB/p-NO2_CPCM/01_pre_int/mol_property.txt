-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1152.6554140670
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 58.0000106709 
   Number of Beta  Electrons                 58.0000106709 
   Total number of  Electrons               116.0000213419 
   Exchange energy                         -117.4627933411 
   Correlation energy                        -3.9881452022 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4509385433 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6554140670 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1157
     Surface Area:         879.2007361311
     Dielectric Energy:     -0.0083571580
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0077715760
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1152.6555055100
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 58.0000086262 
   Number of Beta  Electrons                 58.0000086262 
   Total number of  Electrons               116.0000172525 
   Exchange energy                         -117.4609947265 
   Correlation energy                        -3.9878781696 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4488728961 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6555055100 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1156
     Surface Area:         880.2875823830
     Dielectric Energy:     -0.0084883514
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0077617616
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1152.6553435920
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 57.9999914576 
   Number of Beta  Electrons                 57.9999914576 
   Total number of  Electrons               115.9999829152 
   Exchange energy                         -117.4577713422 
   Correlation energy                        -3.9874939935 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4452653356 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6553435920 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1163
     Surface Area:         882.6213435475
     Dielectric Energy:     -0.0087136794
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0077552409
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1152.6555617013
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 58.0000029040 
   Number of Beta  Electrons                 58.0000029040 
   Total number of  Electrons               116.0000058079 
   Exchange energy                         -117.4590809830 
   Correlation energy                        -3.9876623770 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4467433600 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6555617013 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1161
     Surface Area:         881.6913666406
     Dielectric Energy:     -0.0086626348
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0077508371
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1152.6549147748
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 58.0000008539 
   Number of Beta  Electrons                 58.0000008539 
   Total number of  Electrons               116.0000017078 
   Exchange energy                         -117.4609107409 
   Correlation energy                        -3.9879154323 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4488261732 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6549147748 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1154
     Surface Area:         881.4872670302
     Dielectric Energy:     -0.0085734187
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0077499669
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1152.6555870603
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 58.0000045673 
   Number of Beta  Electrons                 58.0000045673 
   Total number of  Electrons               116.0000091347 
   Exchange energy                         -117.4597140267 
   Correlation energy                        -3.9877652678 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4474792946 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6555870603 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1161
     Surface Area:         881.3338418476
     Dielectric Energy:     -0.0086379517
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0077511907
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1152.6555975407
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 58.0000033587 
   Number of Beta  Electrons                 58.0000033587 
   Total number of  Electrons               116.0000067173 
   Exchange energy                         -117.4597358286 
   Correlation energy                        -3.9877705974 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4475064261 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6555975407 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1158
     Surface Area:         881.4286533097
     Dielectric Energy:     -0.0086503478
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0077509151
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:    -1152.6556068002
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 58.0000029724 
   Number of Beta  Electrons                 58.0000029724 
   Total number of  Electrons               116.0000059448 
   Exchange energy                         -117.4598584441 
   Correlation energy                        -3.9877993436 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4476577876 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6556068002 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 8
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1160
     Surface Area:         881.4590474111
     Dielectric Energy:     -0.0086578294
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0077504724
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:    -1152.6556312273
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 58.0000025302 
   Number of Beta  Electrons                 58.0000025302 
   Total number of  Electrons               116.0000050604 
   Exchange energy                         -117.4601180714 
   Correlation energy                        -3.9878653600 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4479834314 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6556312273 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 9
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1157
     Surface Area:         881.6616543351
     Dielectric Energy:     -0.0086852695
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0077481839
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:    -1152.6556368207
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 58.0000029107 
   Number of Beta  Electrons                 58.0000029107 
   Total number of  Electrons               116.0000058215 
   Exchange energy                         -117.4601259935 
   Correlation energy                        -3.9878763693 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4480023628 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6556368207 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 10
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1158
     Surface Area:         881.7515768227
     Dielectric Energy:     -0.0086911916
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0077473776
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 10
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.6470685372
        Electronic Contribution:
                  0    
      0       5.330730
      1       4.063356
      2       0.639305
        Nuclear Contribution:
                  0    
      0      -5.534852
      1      -3.050395
      2      -0.509724
        Total Dipole moment:
                  0    
      0      -0.204122
      1       1.012961
      2       0.129581
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 11
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 11
   prop. index: 1
        SCF Energy:    -1152.6556368196
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 11
   prop. index: 1
   Number of Alpha Electrons                 58.0000029109 
   Number of Beta  Electrons                 58.0000029109 
   Total number of  Electrons               116.0000058218 
   Exchange energy                         -117.4601305662 
   Correlation energy                        -3.9878763604 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4480069266 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6556368196 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 11
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1158
     Surface Area:         881.7515768227
     Dielectric Energy:     -0.0086913805
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 11
   prop. index: 1
        Van der Waals Correction:       -0.0077473776
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 11
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        227.9840924101
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1152.6453250313
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0119122562
        Number of frequencies          :     57      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      11.918453
      7      27.330935
      8      44.218703
      9      48.028359
     10      50.846598
     11      75.059513
     12      92.583860
     13     133.843885
     14     152.239348
     15     181.762636
     16     245.901772
     17     308.525839
     18     315.884904
     19     408.117265
     20     429.152976
     21     444.438699
     22     484.476866
     23     485.307945
     24     499.483550
     25     540.341990
     26     541.671204
     27     648.472268
     28     685.457411
     29     756.053713
     30     758.645658
     31     838.695536
     32     866.606534
     33     868.433106
     34     977.906667
     35     988.635179
     36     1032.847593
     37     1104.150689
     38     1107.655275
     39     1127.876545
     40     1132.839400
     41     1201.920001
     42     1258.706431
     43     1317.432367
     44     1346.151493
     45     1354.379638
     46     1428.390640
     47     1430.851712
     48     1521.108325
     49     1548.932184
     50     1621.121173
     51     1625.152823
     52     2422.528100
     53     3202.408475
     54     3206.454549
     55     3222.056200
     56     3222.789768
        Zero Point Energy (Hartree)    :          0.1124179923
        Inner Energy (Hartree)         :      -1152.5181622400
        Enthalpy (Hartree)             :      -1152.5172180310
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0157549484
        Vibrational entropy            :          0.0223670102
        Translational entropy          :          0.0157549484
        Entropy                        :          0.0581603580
        Gibbs Energy (Hartree)         :      -1152.5753783890
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.072915851546    0.032071485508   -0.172721386550
               1 C      0.673473638537    1.185953734452   -0.335796834998
               2 C      0.528300053199   -1.078631193702    0.407350730129
               3 C      2.014639951780    1.217144893460    0.083565596084
               4 C      2.602319159582    0.080257051310    0.668165667704
               5 C      1.852969000442   -1.071930797236    0.828979235856
               6 H      0.229277507447    2.067190944690   -0.785523133319
               7 C      2.788538841543    2.389247416134   -0.074152049887
               8 H     -1.108632107627   -0.019352117696   -0.486474923710
               9 N     -0.269759123046   -2.309553523806    0.582182398636
              10 H      3.637457116269    0.122416312656    0.988764916798
              11 H      2.279590529193   -1.961977861097    1.276015403386
              12 N      3.494087298361    3.307674553255   -0.168199424051
              13 O      4.253572306666    4.244154573322   -0.244566171433
              14 O      5.460753726814    1.945114412168    1.468315062939
              15 S      6.420404038088    2.938510232813    0.941210730465
              16 O      7.164356090799    2.563508577476   -0.270255232677
              17 O     -1.433913124346   -2.284521420208    0.204346806849
              18 O      0.285650947845   -3.272837273499    1.093372607781
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.072615455356    0.033120874288   -0.174112449132
               1 C      0.680441216394    1.182101682676   -0.338723139495
               2 C      0.522882805853   -1.080505385558    0.407794544450
               3 C      2.021618855194    1.204823688040    0.081227217988
               4 C      2.603594865546    0.065364227732    0.666863796951
               5 C      1.848048594777   -1.081964065076    0.829517230741
               6 H      0.241490115768    2.065366410804   -0.789693966587
               7 C      2.801289860097    2.372930768297   -0.078585374560
               8 H     -1.108501915391   -0.010306184761   -0.488594590790
               9 N     -0.281453429556   -2.305472972005    0.584906661293
              10 H      3.639172701180    0.100145083999    0.986403483779
              11 H      2.271592201456   -1.972950442747    1.277658781968
              12 N      3.492950429826    3.301542799981   -0.164682712446
              13 O      4.241906192008    4.249202115350   -0.235552634319
              14 O      5.481009563623    1.953210486146    1.461920958637
              15 S      6.425499662750    2.964887017128    0.942508916219
              16 O      7.173052227881    2.604261951528   -0.272277953632
              17 O     -1.447216346462   -2.277684133782    0.210155481236
              18 O      0.265407854413   -3.273633922041    1.097845747700
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.071818925828    0.037348579142   -0.168800453165
               1 C      0.676037547813    1.192577951644   -0.309144857286
               2 C      0.526100528981   -1.081613070710    0.402500735952
               3 C      2.012877224579    1.214116773543    0.124679508695
               4 C      2.597323784712    0.069934164721    0.698422857047
               5 C      1.847854566239   -1.083823322539    0.837205751231
               6 H      0.236237657953    2.080145488286   -0.750705710158
               7 C      2.783928200215    2.391437707199   -0.008000572853
               8 H     -1.104195375070   -0.002085363219   -0.495147891962
               9 N     -0.270576956236   -2.310541644019    0.554477414779
              10 H      3.629812792448    0.102307446540    1.027532780441
              11 H      2.276658620322   -1.976698885934    1.276387745207
              12 N      3.488682964319    3.296946796177   -0.176725321567
              13 O      4.261566343658    4.217222649145   -0.342423979387
              14 O      5.482736471051    2.040596167591    1.518759668358
              15 S      6.434353779817    2.988450113565    0.901595867360
              16 O      7.148007575222    2.503571346403   -0.292787642684
              17 O     -1.431969219088   -2.293590381709    0.161334059655
              18 O      0.276552418891   -3.291862515823    1.045420040335
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     4 
    Coordinates:
               0 C     -0.071638664366    0.034319692563   -0.173624549260
               1 C      0.686438279600    1.181335498635   -0.328963529214
               2 C      0.518431396075   -1.084792008736    0.404122778979
               3 C      2.026051749660    1.194772464370    0.096512531823
               4 C      2.602341696278    0.050358099609    0.677287470863
               5 C      1.842329413601   -1.095169147206    0.830876936933
               6 H      0.252539698723    2.068794300487   -0.776397197316
               7 C      2.809050757400    2.361965834857   -0.054127741559
               8 H     -1.106228566244    0.000650786696   -0.493390647668
               9 N     -0.289282837015   -2.304668814913    0.571267944813
              10 H      3.637043258698    0.076511965862    1.000536855931
              11 H      2.264277970854   -1.988489082399    1.275594976481
              12 N      3.494165611683    3.291423680285   -0.171255631174
              13 O      4.239965747564    4.241589117406   -0.268636861961
              14 O      5.499754181676    1.997499238425    1.478749145872
              15 S      6.431324528840    3.004853916281    0.929073487034
              16 O      7.173565470229    2.612835236928   -0.281082072836
              17 O     -1.457735408205   -2.270599487352    0.201741275195
              18 O      0.247775714949   -3.278751291797    1.086294827061
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     5 
    Coordinates:
               0 C     -0.073447942498    0.035782227775   -0.169109210699
               1 C      0.700426304578    1.168215364295   -0.353865797871
               2 C      0.504404790905   -1.080994224303    0.423941408642
               3 C      2.044700910717    1.169909881650    0.056842384583
               4 C      2.608681997588    0.027710055370    0.653117531613
               5 C      1.832376082285   -1.103185947429    0.835953984531
               6 H      0.275319914426    2.053632114968   -0.813542962502
               7 C      2.845652856642    2.320002742621   -0.126014861079
               8 H     -1.111864155728    0.010663152044   -0.476793562918
               9 N     -0.321518933810   -2.284938333423    0.623799415551
              10 H      3.646974272027    0.045001930798    0.965840637250
              11 H      2.243899880431   -1.994996056872    1.293150204346
              12 N      3.499459111444    3.279062474228   -0.166769215233
              13 O      4.207142728226    4.260602002017   -0.173203979155
              14 O      5.524440436534    1.938657636030    1.415638981418
              15 S      6.424575575395    3.019464560769    0.961330969719
              16 O      7.200920560072    2.755987515903   -0.261095487647
              17 O     -1.475978606136   -2.253603712444    0.214045834498
              18 O      0.224004216901   -3.272533383996    1.101313724953
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     6 
    Coordinates:
               0 C     -0.071041007758    0.034165141622   -0.174037050796
               1 C      0.691411269499    1.177475634633   -0.336472703308
               2 C      0.515084322787   -1.084715280843    0.407805654550
               3 C      2.032111443772    1.187708258394    0.085681894671
               4 C      2.604288539718    0.043898520463    0.671227585157
               5 C      1.839681453196   -1.097931428772    0.831852332545
               6 H      0.260008541578    2.064664264831   -0.786845903993
               7 C      2.819364909202    2.351270717706   -0.069702021370
               8 H     -1.106464156612    0.003053531319   -0.491182108655
               9 N     -0.297850996214   -2.300776994193    0.582575628150
              10 H      3.639479982696    0.067830184608    0.993300867886
              11 H      2.258507162425   -1.990782004603    1.280365832609
              12 N      3.494592754605    3.290771118082   -0.164496106904
              13 O      4.233455744446    4.247169228354   -0.243145381703
              14 O      5.505384140092    1.983364068425    1.463238034741
              15 S      6.428679953870    3.009654492889    0.934982490825
              16 O      7.177915707781    2.649546264600   -0.280446068576
              17 O     -1.464038196933   -2.265232033057    0.206740264303
              18 O      0.239598431851   -3.276693684457    1.093136759868
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     7 
    Coordinates:
               0 C     -0.071112434781    0.034665937173   -0.172735960852
               1 C      0.690087974641    1.179346659213   -0.331116967682
               2 C      0.515638470666   -1.085159388445    0.406767061010
               3 C      2.030161953696    1.190089331540    0.093114351549
               4 C      2.603142165160    0.045229378433    0.675754622221
               5 C      1.839796759005   -1.098018483899    0.832216725030
               6 H      0.258193064162    2.067113851789   -0.779873643984
               7 C      2.816462898288    2.354660048825   -0.059045121598
               8 H     -1.106074659273    0.003517625452   -0.491431210512
               9 N     -0.295935717146   -2.302591342678    0.577115010553
              10 H      3.638025851338    0.069066816131    0.998737176128
              11 H      2.259574074979   -1.991521444764    1.278560226610
              12 N      3.495994477122    3.289133210988   -0.170245569744
              13 O      4.238479381913    4.241957750653   -0.259528054260
              14 O      5.503490309990    1.995448640445    1.472092208393
              15 S      6.428752529597    3.012690956551    0.929851874014
              16 O      7.175482436001    2.635440490515   -0.282040906143
              17 O     -1.461963962618   -2.267295427722    0.200794009046
              18 O      0.241974427260   -3.279334610200    1.085594170219
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     8 
    Coordinates:
               0 C     -0.070927575871    0.035012219824   -0.171976584666
               1 C      0.691052055745    1.179267461570   -0.329727708724
               2 C      0.514973543626   -1.085725929052    0.406585839294
               3 C      2.031298115348    1.188667345720    0.094088155870
               4 C      2.603420784862    0.042971497068    0.675796287821
               5 C      1.839223550919   -1.099861523166    0.831585313828
               6 H      0.259656730541    2.067652355222   -0.777729707583
               7 C      2.818638351814    2.352502245242   -0.057579604244
               8 H     -1.105988146847    0.005097921498   -0.490473564440
               9 N     -0.297628108784   -2.302588046733    0.576398004253
              10 H      3.638384950649    0.065594566872    0.998645417831
              11 H      2.258609143940   -1.993923000108    1.277171218009
              12 N      3.496699783532    3.287697376830   -0.171655502600
              13 O      4.239851651033    4.239690807828   -0.264067367704
              14 O      5.504157142221    2.001342198712    1.474965223022
              15 S      6.427560751254    3.018383217617    0.929086256200
              16 O      7.175220068839    2.638347134553   -0.281390014287
              17 O     -1.463850513671   -2.265638750933    0.200990868511
              18 O      0.239817720850   -3.280049098565    1.083867469609
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     9 
    Coordinates:
               0 C     -0.070822320131    0.036209991485   -0.169323933236
               1 C      0.694635181172    1.178084950040   -0.327434710526
               2 C      0.512072992276   -1.087339072999    0.406767594727
               3 C      2.035856481915    1.182378640551    0.093588087160
               4 C      2.604911325062    0.034105300267    0.672912390442
               5 C      1.837053327649   -1.106377149772    0.828961881379
               6 H      0.265194515910    2.068394767088   -0.773463890244
               7 C      2.827245151352    2.343235332133   -0.057987945775
               8 H     -1.106616149282    0.010744509267   -0.485813234002
               9 N     -0.304713967829   -2.301578866463    0.577043064833
              10 H      3.640490828132    0.052437815120    0.994214699696
              11 H      2.254661595074   -2.002134747520    1.272791022507
              12 N      3.499638985224    3.281876505079   -0.177276393173
              13 O      4.241968520063    4.234366784363   -0.271749260991
              14 O      5.508271997883    2.015259960858    1.480436342185
              15 S      6.423583601834    3.037644148465    0.930545905960
              16 O      7.177640698415    2.657932866484   -0.276129033466
              17 O     -1.471770812215   -2.259241777941    0.205145603739
              18 O      0.230868047496   -3.281559956505    1.081351808787
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     10 
    Coordinates:
               0 C     -0.071254621105    0.036646296701   -0.168052416964
               1 C      0.695164986054    1.177721141654   -0.327184530971
               2 C      0.511260440105   -1.087561463720    0.407105705432
               3 C      2.036988151075    1.180557254461    0.091896681772
               4 C      2.605572618608    0.031685272315    0.670523105404
               5 C      1.836737472212   -1.108000871747    0.827570387371
               6 H      0.266044002519    2.068498551642   -0.772606081428
               7 C      2.829522401531    2.340522885576   -0.060463424708
               8 H     -1.107528439756    0.012137127563   -0.483074420637
               9 N     -0.306651774244   -2.301123338377    0.578256103606
              10 H      3.641571320705    0.049062520537    0.990551510484
              11 H      2.253872048273   -2.004270644953    1.270832720710
              12 N      3.500632670292    3.280101477576   -0.179757014118
              13 O      4.242839348377    4.232792962928   -0.273345714818
              14 O      5.509272297794    2.017663856726    1.482282744871
              15 S      6.422137096380    3.042660906259    0.933040167629
              16 O      7.179332388979    2.664575168513   -0.272155794821
              17 O     -1.474153494112   -2.257351730272    0.208069047436
              18 O      0.228811086314   -3.281877373382    1.081091223748
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     11 
    Coordinates:
               0 C     -0.071254621105    0.036646296701   -0.168052416964
               1 C      0.695164986054    1.177721141654   -0.327184530971
               2 C      0.511260440105   -1.087561463720    0.407105705432
               3 C      2.036988151075    1.180557254461    0.091896681772
               4 C      2.605572618608    0.031685272315    0.670523105404
               5 C      1.836737472212   -1.108000871747    0.827570387371
               6 H      0.266044002519    2.068498551642   -0.772606081428
               7 C      2.829522401531    2.340522885576   -0.060463424708
               8 H     -1.107528439756    0.012137127563   -0.483074420637
               9 N     -0.306651774244   -2.301123338377    0.578256103606
              10 H      3.641571320705    0.049062520537    0.990551510484
              11 H      2.253872048273   -2.004270644953    1.270832720710
              12 N      3.500632670292    3.280101477576   -0.179757014118
              13 O      4.242839348377    4.232792962928   -0.273345714818
              14 O      5.509272297794    2.017663856726    1.482282744871
              15 S      6.422137096380    3.042660906259    0.933040167629
              16 O      7.179332388979    2.664575168513   -0.272155794821
              17 O     -1.474153494112   -2.257351730272    0.208069047436
              18 O      0.228811086314   -3.281877373382    1.081091223748
