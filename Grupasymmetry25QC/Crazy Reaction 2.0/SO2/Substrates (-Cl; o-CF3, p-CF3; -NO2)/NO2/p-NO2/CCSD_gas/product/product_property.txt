-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -600.9843223697
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1207     6.0000    -0.1207     3.8491     3.8491     0.0000
  1   0     6.1811     6.0000    -0.1811     3.7815     3.7815    -0.0000
  2   0     5.9783     6.0000     0.0217     3.7984     3.7984    -0.0000
  3   0     5.7632     6.0000     0.2368     3.8814     3.8814    -0.0000
  4   0     6.2166     6.0000    -0.2166     3.7444     3.7444     0.0000
  5   0     6.1236     6.0000    -0.1236     3.8637     3.8637    -0.0000
  6   0     0.8278     1.0000     0.1722     0.9858     0.9858    -0.0000
  7   0     5.6017     6.0000     0.3983     4.2701     4.2701    -0.0000
  8   0     0.7948     1.0000     0.2052     0.9767     0.9767     0.0000
  9   0     6.5146     7.0000     0.4854     4.2451     4.2451    -0.0000
 10   0     0.8395     1.0000     0.1605     0.9815     0.9815     0.0000
 11   0     0.7940     1.0000     0.2060     0.9784     0.9784    -0.0000
 12   0     7.2267     7.0000    -0.2267     3.0856     3.0856    -0.0000
 13   0     8.2958     8.0000    -0.2958     2.3055     2.3055    -0.0000
 14   0     8.3603     8.0000    -0.3603     1.8933     1.8933    -0.0000
 15   0     8.3612     8.0000    -0.3612     1.8930     1.8930    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.375984
                0             6               2            6                1.376326
                0             6               8            1                0.943473
                1             6               3            6                1.354279
                1             6               6            1                0.967077
                2             6               5            6                1.380539
                2             6               9            7                0.957470
                3             6               4            6                1.314786
                3             6              12            7                1.072719
                4             6               5            6                1.376197
                4             6              10            1                0.976733
                5             6              11            1                0.941831
                7             6              12            7                1.970854
                7             6              13            8                2.203911
                9             7              14            8                1.648481
                9             7              15            8                1.647701
               14             8              15            8                0.157991
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.9843223746
        Total Correlation Energy:                                          -2.2798576636
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1121601839
        Total MDCI Energy:                                               -603.2641800382
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -601.0257921354
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 17
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0795     6.0000    -0.0795     3.8308     3.8308    -0.0000
  1   0     6.1572     6.0000    -0.1572     3.7859     3.7859    -0.0000
  2   0     6.0306     6.0000    -0.0306     3.7959     3.7959    -0.0000
  3   0     5.7427     6.0000     0.2573     3.8366     3.8366     0.0000
  4   0     6.1690     6.0000    -0.1690     3.7604     3.7604    -0.0000
  5   0     6.0976     6.0000    -0.0976     3.8360     3.8360    -0.0000
  6   0     0.8199     1.0000     0.1801     0.9901     0.9901     0.0000
  7   0     5.3592     6.0000     0.6408     4.2803     4.2803     0.0000
  8   0     0.7778     1.0000     0.2222     0.9732     0.9732     0.0000
  9   0     6.5510     7.0000     0.4490     4.2938     4.2938     0.0000
 10   0     0.8604     1.0000     0.1396     0.9978     0.9978    -0.0000
 11   0     0.7771     1.0000     0.2229     0.9764     0.9764     0.0000
 12   0     7.4954     7.0000    -0.4954     3.1289     3.1289     0.0000
 13   0     8.3543     8.0000    -0.3543     2.2859     2.2859     0.0000
 14   0     8.3637     8.0000    -0.3637     1.9003     1.9003     0.0000
 15   0     8.3645     8.0000    -0.3645     1.8989     1.8989     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.389900
                0             6               2            6                1.375567
                0             6               8            1                0.937223
                1             6               3            6                1.363069
                1             6               6            1                0.975281
                2             6               5            6                1.372959
                2             6               9            7                0.983877
                3             6               4            6                1.314949
                3             6              12            7                1.058309
                4             6               5            6                1.380841
                4             6              10            1                1.006408
                5             6              11            1                0.937523
                7             6              12            7                2.042062
                7             6              13            8                2.161654
                9             7              14            8                1.673158
                9             7              15            8                1.671475
               14             8              15            8                0.152685
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -601.0257921288
        Total Correlation Energy:                                          -2.4168287958
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1218538548
        Total MDCI Energy:                                               -603.4426209246
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -600.9843223746 
       SCF energy with basis  cc-pVQZ :   -601.0257921288 
       CBS SCF Energy                    :   -601.0382871285 
       Correlation energy with basis  cc-pVTZ :   -600.9843223746 
       Correlation energy with basis  cc-pVQZ :   -601.0257921288 
       CBS Correlation Energy            :     -2.5143371044 
       CBS Total Energy                  :   -603.5526242328 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       16
     number of electrons:                   84
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             416
     number of aux C basis functions:       1804
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14562-aKPQ/product.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.0813891701
        Electronic Contribution:
                  0    
      0       1.527415
      1       0.338938
      2      -0.287987
        Nuclear Contribution:
                  0    
      0      -0.625015
      1       0.413906
      2      -0.009570
        Total Dipole moment:
                  0    
      0       0.902400
      1       0.752845
      2      -0.297557
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C      1.773300000000   -0.420480000000    0.367644000000
               1 C      2.857913000000    0.372844000000    0.033862000000
               2 C      1.983554000000   -1.764051000000    0.654471000000
               3 C      4.140798000000   -0.182092000000   -0.010618000000
               4 C      4.334332000000   -1.539242000000    0.281767000000
               5 C      3.251920000000   -2.333539000000    0.615673000000
               6 H      2.727398000000    1.424858000000   -0.195139000000
               7 C      6.396801000000    0.521143000000   -0.484047000000
               8 H      0.769848000000   -0.014008000000    0.408737000000
               9 N      0.832231000000   -2.609341000000    1.009651000000
              10 H      5.333802000000   -1.962748000000    0.245153000000
              11 H      3.375492000000   -3.385215000000    0.845718000000
              12 N      5.199867000000    0.653124000000   -0.351461000000
              13 O      7.553011000000    0.536578000000   -0.648377000000
              14 O     -0.272753000000   -2.080519000000    1.033200000000
              15 O      1.051397000000   -3.788833000000    1.259365000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C      1.773300000000   -0.420480000000    0.367644000000
               1 C      2.857913000000    0.372844000000    0.033862000000
               2 C      1.983554000000   -1.764051000000    0.654471000000
               3 C      4.140798000000   -0.182092000000   -0.010618000000
               4 C      4.334332000000   -1.539242000000    0.281767000000
               5 C      3.251920000000   -2.333539000000    0.615673000000
               6 H      2.727398000000    1.424858000000   -0.195139000000
               7 C      6.396801000000    0.521143000000   -0.484047000000
               8 H      0.769848000000   -0.014008000000    0.408737000000
               9 N      0.832231000000   -2.609341000000    1.009651000000
              10 H      5.333802000000   -1.962748000000    0.245153000000
              11 H      3.375492000000   -3.385215000000    0.845718000000
              12 N      5.199867000000    0.653124000000   -0.351461000000
              13 O      7.553011000000    0.536578000000   -0.648377000000
              14 O     -0.272753000000   -2.080519000000    1.033200000000
              15 O      1.051397000000   -3.788833000000    1.259365000000
