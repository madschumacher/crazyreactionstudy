-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1133214542
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1104
     Surface Area:         842.3839584345
     Dielectric Energy:     -0.0121944732
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1344     6.0000    -0.1344     3.8482     3.8482     0.0000
  1   0     6.1312     6.0000    -0.1312     3.8084     3.8084     0.0000
  2   0     5.9329     6.0000     0.0671     3.7792     3.7792     0.0000
  3   0     5.9038     6.0000     0.0962     3.7746     3.7746    -0.0000
  4   0     6.1459     6.0000    -0.1459     3.7961     3.7961     0.0000
  5   0     6.1314     6.0000    -0.1314     3.8402     3.8402    -0.0000
  6   0     0.8118     1.0000     0.1882     0.9761     0.9761     0.0000
  7   0     5.8181     6.0000     0.1819     3.8129     3.8129     0.0000
  8   0     0.7871     1.0000     0.2129     0.9733     0.9733     0.0000
  9   0     6.5046     7.0000     0.4954     4.2403     4.2403    -0.0000
 10   0     0.8042     1.0000     0.1958     0.9894     0.9894     0.0000
 11   0     0.7868     1.0000     0.2132     0.9735     0.9735    -0.0000
 12   0     6.8146     7.0000     0.1854     3.6342     3.6342     0.0000
 13   0     8.4665     8.0000    -0.4665     1.7730     1.7730     0.0000
 14   0     8.5501     8.0000    -0.5501     1.9448     1.9448     0.0000
 15   0    15.0267    16.0000     0.9733     3.8447     3.8447     0.0000
 16   0     8.5118     8.0000    -0.5118     1.9970     1.9970    -0.0000
 17   0     8.3698     8.0000    -0.3698     1.8870     1.8870    -0.0000
 18   0     8.3683     8.0000    -0.3683     1.8892     1.8892    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.391491
                0             6               2            6                1.359690
                0             6               8            1                0.943787
                1             6               3            6                1.294631
                1             6               6            1                0.965253
                2             6               5            6                1.377248
                2             6               9            7                0.950021
                3             6               4            6                1.340087
                3             6               7            6                1.069119
                4             6               5            6                1.365144
                4             6              10            1                0.951641
                5             6              11            1                0.945406
                7             6              12            7                2.290000
                7             6              13            8                0.177986
                7             6              14            8                0.223033
                9             7              17            8                1.645928
                9             7              18            8                1.648608
               12             7              13            8                1.229005
               13             8              15           16                0.288664
               14             8              15           16                1.619218
               15            16              16            8                1.877752
               17             8              18            8                0.158987
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1133213978
        Total Correlation Energy:                                          -3.0142879285
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1539613593
        Total MDCI Energy:                                              -1151.1276093264
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.1769139151
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1104
     Surface Area:         842.3839584345
     Dielectric Energy:     -0.0121366191
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0784     6.0000    -0.0784     3.8131     3.8131    -0.0000
  1   0     6.1225     6.0000    -0.1225     3.8160     3.8160    -0.0000
  2   0     6.0062     6.0000    -0.0062     3.7949     3.7949     0.0000
  3   0     5.9834     6.0000     0.0166     3.7905     3.7905    -0.0000
  4   0     6.1532     6.0000    -0.1532     3.7734     3.7734    -0.0000
  5   0     6.0661     6.0000    -0.0661     3.8035     3.8035    -0.0000
  6   0     0.8085     1.0000     0.1915     0.9820     0.9820    -0.0000
  7   0     5.6969     6.0000     0.3031     3.7700     3.7700     0.0000
  8   0     0.7713     1.0000     0.2287     0.9716     0.9716    -0.0000
  9   0     6.5346     7.0000     0.4654     4.2717     4.2717     0.0000
 10   0     0.7993     1.0000     0.2007     0.9965     0.9965    -0.0000
 11   0     0.7716     1.0000     0.2284     0.9714     0.9714    -0.0000
 12   0     6.8918     7.0000     0.1082     3.5761     3.5761     0.0000
 13   0     8.4574     8.0000    -0.4574     1.7950     1.7950     0.0000
 14   0     8.5954     8.0000    -0.5954     1.8986     1.8986     0.0000
 15   0    14.9399    16.0000     1.0601     3.8250     3.8250    -0.0000
 16   0     8.5733     8.0000    -0.5733     1.9302     1.9302    -0.0000
 17   0     8.3760     8.0000    -0.3760     1.8865     1.8865    -0.0000
 18   0     8.3742     8.0000    -0.3742     1.8888     1.8888     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.412676
                0             6               2            6                1.358245
                0             6               8            1                0.941902
                1             6               3            6                1.269343
                1             6               6            1                0.976193
                2             6               5            6                1.377239
                2             6               9            7                0.976966
                3             6               4            6                1.306503
                3             6               7            6                1.163242
                4             6               5            6                1.389664
                4             6              10            1                0.955755
                5             6              11            1                0.942500
                7             6              12            7                2.207994
                7             6              13            8                0.156082
                7             6              14            8                0.242117
                9             7              17            8                1.663070
                9             7              18            8                1.665794
               12             7              13            8                1.242057
               13             8              15           16                0.312110
               14             8              15           16                1.606403
               15            16              16            8                1.844378
               17             8              18            8                0.153979
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1769140196
        Total Correlation Energy:                                          -3.2033870261
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1681880711
        Total MDCI Energy:                                              -1151.3803010458
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1133213978 
       SCF energy with basis  cc-pVQZ :  -1148.1769140196 
       CBS SCF Energy                    :  -1148.1960747261 
       Correlation energy with basis  cc-pVTZ :  -1148.1133213978 
       Correlation energy with basis  cc-pVQZ :  -1148.1769140196 
       CBS Correlation Energy            :     -3.3380046850 
       CBS Total Energy                  :  -1151.5340794111 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14571-mt5M/ts1_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.1230430735
        Electronic Contribution:
                  0    
      0       4.525895
      1       3.767672
      2       0.933505
        Nuclear Contribution:
                  0    
      0      -4.791146
      1      -3.240431
      2      -0.342484
        Total Dipole moment:
                  0    
      0      -0.265251
      1       0.527241
      2       0.591021
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.396744000000   -0.767407000000   -0.454934000000
               1 C     -1.523143000000    0.300823000000   -0.535973000000
               2 C     -1.944656000000   -1.965868000000    0.088143000000
               3 C     -0.199494000000    0.151742000000   -0.080458000000
               4 C      0.236212000000   -1.065859000000    0.463348000000
               5 C     -0.644371000000   -2.130838000000    0.549126000000
               6 H     -1.850131000000    1.248461000000   -0.949849000000
               7 C      0.695760000000    1.249590000000   -0.173443000000
               8 H     -3.419586000000   -0.677790000000   -0.800877000000
               9 N     -2.880779000000   -3.100795000000    0.178622000000
              10 H      1.257023000000   -1.162687000000    0.812665000000
              11 H     -0.330194000000   -3.080841000000    0.964314000000
              12 N      0.934387000000    2.335625000000   -0.634621000000
              13 O      1.800799000000    3.224879000000   -0.581885000000
              14 O      2.298881000000    1.103496000000    1.069491000000
              15 S      3.393283000000    2.080926000000    0.659589000000
              16 O      4.280051000000    1.578493000000   -0.408373000000
              17 O     -4.026800000000   -2.925122000000   -0.218289000000
              18 O     -2.457489000000   -4.151499000000    0.644936000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.396744000000   -0.767407000000   -0.454934000000
               1 C     -1.523143000000    0.300823000000   -0.535973000000
               2 C     -1.944656000000   -1.965868000000    0.088143000000
               3 C     -0.199494000000    0.151742000000   -0.080458000000
               4 C      0.236212000000   -1.065859000000    0.463348000000
               5 C     -0.644371000000   -2.130838000000    0.549126000000
               6 H     -1.850131000000    1.248461000000   -0.949849000000
               7 C      0.695760000000    1.249590000000   -0.173443000000
               8 H     -3.419586000000   -0.677790000000   -0.800877000000
               9 N     -2.880779000000   -3.100795000000    0.178622000000
              10 H      1.257023000000   -1.162687000000    0.812665000000
              11 H     -0.330194000000   -3.080841000000    0.964314000000
              12 N      0.934387000000    2.335625000000   -0.634621000000
              13 O      1.800799000000    3.224879000000   -0.581885000000
              14 O      2.298881000000    1.103496000000    1.069491000000
              15 S      3.393283000000    2.080926000000    0.659589000000
              16 O      4.280051000000    1.578493000000   -0.408373000000
              17 O     -4.026800000000   -2.925122000000   -0.218289000000
              18 O     -2.457489000000   -4.151499000000    0.644936000000
