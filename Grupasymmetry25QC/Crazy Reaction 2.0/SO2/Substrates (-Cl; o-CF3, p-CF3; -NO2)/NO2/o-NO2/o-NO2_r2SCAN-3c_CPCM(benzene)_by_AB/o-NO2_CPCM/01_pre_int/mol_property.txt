-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1152.6518707632
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 58.0000283373 
   Number of Beta  Electrons                 58.0000283373 
   Total number of  Electrons               116.0000566745 
   Exchange energy                         -117.4629806856 
   Correlation energy                        -3.9907288818 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4537095675 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6518707632 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1142
     Surface Area:         858.8896124259
     Dielectric Energy:     -0.0081728519
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0080883905
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1152.6519427471
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 58.0000280947 
   Number of Beta  Electrons                 58.0000280947 
   Total number of  Electrons               116.0000561894 
   Exchange energy                         -117.4604619033 
   Correlation energy                        -3.9904101327 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4508720361 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6519427471 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1145
     Surface Area:         859.6901990639
     Dielectric Energy:     -0.0082839156
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0080773700
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1152.6519718365
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 58.0000271020 
   Number of Beta  Electrons                 58.0000271020 
   Total number of  Electrons               116.0000542040 
   Exchange energy                         -117.4589920721 
   Correlation energy                        -3.9902398272 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4492318993 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6519718365 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1148
     Surface Area:         860.1891259528
     Dielectric Energy:     -0.0083856155
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0080698002
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1152.6519224363
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 58.0000255145 
   Number of Beta  Electrons                 58.0000255145 
   Total number of  Electrons               116.0000510289 
   Exchange energy                         -117.4571866435 
   Correlation energy                        -3.9900459860 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4472326295 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6519224363 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1150
     Surface Area:         861.5754117892
     Dielectric Energy:     -0.0085528151
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0080584231
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1152.6520087499
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 58.0000279807 
   Number of Beta  Electrons                 58.0000279807 
   Total number of  Electrons               116.0000559614 
   Exchange energy                         -117.4592260533 
   Correlation energy                        -3.9902716276 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4494976809 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520087499 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1147
     Surface Area:         860.2774861813
     Dielectric Energy:     -0.0084163414
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0080670941
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1152.6520090556
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 58.0000275050 
   Number of Beta  Electrons                 58.0000275050 
   Total number of  Electrons               116.0000550099 
   Exchange energy                         -117.4595566087 
   Correlation energy                        -3.9903187119 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4498753206 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520090556 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1149
     Surface Area:         860.1542871966
     Dielectric Energy:     -0.0083968969
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0080689400
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1152.6520201761
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 58.0000279030 
   Number of Beta  Electrons                 58.0000279030 
   Total number of  Electrons               116.0000558059 
   Exchange energy                         -117.4596253085 
   Correlation energy                        -3.9903242196 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4499495281 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520201761 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1148
     Surface Area:         860.1883470991
     Dielectric Energy:     -0.0084100906
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0080675711
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:    -1152.6520355400
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 58.0000282831 
   Number of Beta  Electrons                 58.0000282831 
   Total number of  Electrons               116.0000565661 
   Exchange energy                         -117.4598773099 
   Correlation energy                        -3.9903468819 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4502241919 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520355400 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 8
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1151
     Surface Area:         860.2737858019
     Dielectric Energy:     -0.0084291902
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0080650391
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:    -1152.6520396519
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 58.0000282620 
   Number of Beta  Electrons                 58.0000282620 
   Total number of  Electrons               116.0000565240 
   Exchange energy                         -117.4599804851 
   Correlation energy                        -3.9903500271 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4503305122 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520396519 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 9
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1153
     Surface Area:         860.3688173633
     Dielectric Energy:     -0.0084360867
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0080633631
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:    -1152.6520392845
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 58.0000281406 
   Number of Beta  Electrons                 58.0000281406 
   Total number of  Electrons               116.0000562813 
   Exchange energy                         -117.4599931648 
   Correlation energy                        -3.9903427232 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4503358880 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520392845 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 10
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1153
     Surface Area:         860.4475447348
     Dielectric Energy:     -0.0084374001
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0080622570
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 10
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.2958576287
        Electronic Contribution:
                  0    
      0       5.231107
      1       8.376079
      2      -0.870659
        Nuclear Contribution:
                  0    
      0      -5.796228
      1     -10.517460
      2       1.979853
        Total Dipole moment:
                  0    
      0      -0.565121
      1      -2.141381
      2       1.109194
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 11
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 11
   prop. index: 1
        SCF Energy:    -1152.6520392918
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 11
   prop. index: 1
   Number of Alpha Electrons                 58.0000281406 
   Number of Beta  Electrons                 58.0000281406 
   Total number of  Electrons               116.0000562812 
   Exchange energy                         -117.4599925154 
   Correlation energy                        -3.9903427617 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -121.4503352771 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1152.6520392918 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 11
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1153
     Surface Area:         860.4475447348
     Dielectric Energy:     -0.0084373240
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 11
   prop. index: 1
        Van der Waals Correction:       -0.0080622570
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 11
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        227.9840924101
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1152.6418851721
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0117745765
        Number of frequencies          :     57      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      18.633822
      7      35.900412
      8      39.963619
      9      57.818247
     10      66.944576
     11      83.820420
     12     107.144255
     13     149.831373
     14     159.850199
     15     190.837508
     16     237.712835
     17     270.496251
     18     356.613899
     19     388.195602
     20     428.828696
     21     451.446992
     22     453.992680
     23     498.379052
     24     499.430881
     25     511.462311
     26     569.352656
     27     669.824706
     28     675.438889
     29     743.550775
     30     760.361044
     31     800.947276
     32     870.908949
     33     892.998796
     34     982.794371
     35     1004.861516
     36     1056.690206
     37     1091.023934
     38     1104.193363
     39     1128.552481
     40     1171.303801
     41     1188.379497
     42     1255.723254
     43     1278.585518
     44     1339.778199
     45     1362.074637
     46     1423.115412
     47     1465.316832
     48     1517.247187
     49     1552.103767
     50     1596.092516
     51     1638.330853
     52     2412.944863
     53     3187.050876
     54     3199.180012
     55     3208.536506
     56     3221.106276
        Zero Point Energy (Hartree)    :          0.1124860590
        Inner Energy (Hartree)         :      -1152.5147919938
        Enthalpy (Hartree)             :      -1152.5138477847
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0156350301
        Vibrational entropy            :          0.0217603626
        Translational entropy          :          0.0156350301
        Entropy                        :          0.0574337921
        Gibbs Energy (Hartree)         :      -1152.5712815768
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.021905000000   -0.095590000000   -0.096478000000
               1 C      0.707403000000    1.095729000000   -0.288140000000
               2 C      0.677548000000   -1.174812000000    0.477468000000
               3 C      2.055536000000    1.239118000000    0.085585000000
               4 C      2.701055000000    0.131390000000    0.666605000000
               5 C      2.014396000000   -1.056634000000    0.856192000000
               6 N     -0.012221000000    2.223217000000   -0.899457000000
               7 C      2.793334000000    2.428623000000   -0.088695000000
               8 H     -1.016005000000   -0.155856000000   -0.401253000000
               9 H      0.145086000000   -2.107948000000    0.629100000000
              10 H      3.739431000000    0.236068000000    0.961589000000
              11 H      2.529583000000   -1.899851000000    1.305782000000
              12 N      3.606194000000    3.258196000000   -0.132314000000
              13 O      4.436812000000    4.135735000000   -0.181236000000
              14 O      5.506255000000    1.847463000000    1.562461000000
              15 S      6.483895000000    2.815106000000    1.017114000000
              16 O      7.232422000000    2.391702000000   -0.176408000000
              17 O     -1.183719000000    2.050208000000   -1.206113000000
              18 O      0.609581000000    3.267736000000   -1.063043000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.017080309442   -0.095602961290   -0.095295610480
               1 C      0.706687911633    1.093697337263   -0.287061480531
               2 C      0.669795830661   -1.176838939709    0.478352866190
               3 C      2.055622606563    1.232716234826    0.086437355207
               4 C      2.697884434561    0.123024602201    0.665994533116
               5 C      2.007424760249   -1.063177895311    0.856022378015
               6 N     -0.007262272257    2.223263424674   -0.898287383531
               7 C      2.796603319476    2.420736136767   -0.088036115032
               8 H     -1.021126718635   -0.154709045467   -0.399345678574
               9 H      0.134526302120   -2.108346012655    0.629998997515
              10 H      3.737168509920    0.222682163065    0.958564562778
              11 H      2.520322246869   -1.908373508854    1.304572807495
              12 N      3.607367421433    3.251608787682   -0.133359293458
              13 O      4.432371752368    4.137163516060   -0.184031470710
              14 O      5.523538280753    1.858514729123    1.563906292141
              15 S      6.491283335667    2.835219672259    1.017694778063
              16 O      7.241950999300    2.412709603937   -0.176017591365
              17 O     -1.179840526033    2.058719568247   -1.207382556745
              18 O      0.617092495910    3.266592587181   -1.063968390094
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     3 
    Coordinates:
               0 C      0.013664501802   -0.095307670182   -0.096487064579
               1 C      0.707044147922    1.091863540363   -0.288210923038
               2 C      0.663498428343   -1.178295259799    0.477454801620
               3 C      2.056684759676    1.226759580851    0.085734312358
               4 C      2.695432447997    0.115695125480    0.664980011406
               5 C      2.001451752785   -1.068824733372    0.855196929118
               6 N     -0.001843404501    2.221783775274   -0.899139868935
               7 C      2.800872430789    2.413140872696   -0.087330025816
               8 H     -1.024745518141   -0.154047981897   -0.400011268305
               9 H      0.125417463339   -2.108138337716    0.629260302976
              10 H      3.735279958949    0.210579713955    0.957265990241
              11 H      2.511721941752   -1.915523390260    1.303845139180
              12 N      3.608808318355    3.246172562667   -0.133283812762
              13 O      4.433037730943    4.133937069381   -0.193657998811
              14 O      5.535913565082    1.875478703310    1.570516177625
              15 S      6.495607109799    2.855604467203    1.017093978018
              16 O      7.246879127007    2.425616851471   -0.174948538323
              17 O     -1.177977312417    2.065624335491   -1.201542269787
              18 O      0.621743550517    3.267480775085   -1.057976872188
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     4 
    Coordinates:
               0 C      0.005747407822   -0.092011635249   -0.092432150304
               1 C      0.705367773318    1.091792434206   -0.282279896623
               2 C      0.651571124473   -1.179866206830    0.477477919946
               3 C      2.057429603995    1.217594622505    0.088759766585
               4 C      2.691040466667    0.102402266435    0.663074974453
               5 C      1.990985624086   -1.079346663343    0.852237798684
               6 N      0.003589529850    2.224264242322   -0.886675185599
               7 C      2.807613335786    2.400797628876   -0.080567281645
               8 H     -1.033833320629   -0.148904854230   -0.392317631885
               9 H      0.108525507297   -2.106919029809    0.628551735527
              10 H      3.732491628388    0.187803100165    0.953137909511
              11 H      2.497395459286   -1.929982593807    1.297649664907
              12 N      3.610976285074    3.237313844044   -0.127763185922
              13 O      4.431888391582    4.130499253952   -0.196383629765
              14 O      5.559922638356    1.895513509380    1.573082415072
              15 S      6.502401880921    2.888775478583    1.014843632674
              16 O      7.255077772044    2.459461799367   -0.178866633703
              17 O     -1.167665675641    2.069628046256   -1.212388637352
              18 O      0.637965567324    3.260784757174   -1.070382584561
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     5 
    Coordinates:
               0 C      0.010447857312   -0.093621511504   -0.094268480082
               1 C      0.706377832297    1.091878583350   -0.286784154065
               2 C      0.658393988286   -1.178252089125    0.479028939367
               3 C      2.057100962605    1.222875426987    0.085485071356
               4 C      2.693654726181    0.110437830711    0.663975836721
               5 C      1.996982758417   -1.072454564260    0.855150075844
               6 N      0.000075878269    2.222817894052   -0.896232535460
               7 C      2.804613247071    2.406851914935   -0.088080475554
               8 H     -1.028463146917   -0.151158642986   -0.396052582524
               9 H      0.118000782739   -2.106642385522    0.631590209389
              10 H      3.734097540960    0.201828766257    0.955579982504
              11 H      2.505553160182   -1.920386609253    1.303335250944
              12 N      3.611917891798    3.240237399420   -0.135911960794
              13 O      4.431743774411    4.132591648088   -0.187109499361
              14 O      5.544271325888    1.873757288140    1.563424979369
              15 S      6.495981620139    2.866467385134    1.018887336630
              16 O      7.253878388287    2.451007330240   -0.174587544420
              17 O     -1.174772933767    2.066924398221   -1.204781755186
              18 O      0.628635345843    3.264439937113   -1.063889694679
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     6 
    Coordinates:
               0 C      0.011896493848   -0.094084737694   -0.094876421456
               1 C      0.706354129362    1.092410213600   -0.286717397514
               2 C      0.660882106233   -1.178404250291    0.477813626380
               3 C      2.056785673890    1.224629484904    0.086026746751
               4 C      2.694514855150    0.112480289810    0.664138545514
               5 C      1.999197500606   -1.071290571346    0.854415235791
               6 N     -0.001368382340    2.223416900053   -0.895808909644
               7 C      2.803352142987    2.409225380634   -0.086804311938
               8 H     -1.026872615231   -0.152320063772   -0.396953766663
               9 H      0.121497085651   -2.107535245023    0.629476073184
              10 H      3.734699383838    0.205098300183    0.956164764696
              11 H      2.508699902006   -1.918935302500    1.302106219937
              12 N      3.610036207242    3.243379405522   -0.134036760100
              13 O      4.435433045361    4.129539557897   -0.193756072533
              14 O      5.539519662212    1.877685096401    1.569516134700
              15 S      6.494410841524    2.863369336720    1.017524577574
              16 O      7.249259445808    2.438905098609   -0.174391419152
              17 O     -1.176052589214    2.066345368752   -1.203663042551
              18 O      0.626246111066    3.265685737543   -1.061414822976
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     7 
    Coordinates:
               0 C      0.010725369042   -0.093630308479   -0.094369648776
               1 C      0.706156317041    1.092180752885   -0.286758021842
               2 C      0.658895364100   -1.178441869843    0.478392546120
               3 C      2.056926258820    1.223042264155    0.085520278115
               4 C      2.693753262337    0.110524307598    0.663724930845
               5 C      1.997405577548   -1.072602945654    0.854567945371
               6 N     -0.000600338680    2.223474801778   -0.895762014959
               7 C      2.804972322902    2.406602212832   -0.087599906109
               8 H     -1.028232299384   -0.151448580547   -0.395913375444
               9 H      0.118587725270   -2.106969407408    0.630461052820
              10 H      3.734127258592    0.201971864157    0.955587265792
              11 H      2.506226608996   -1.920590382178    1.302365630135
              12 N      3.611269340617    3.240974217666   -0.135705540093
              13 O      4.435624747971    4.128430623478   -0.191893938503
              14 O      5.543060162248    1.876679955050    1.566848131439
              15 S      6.494565788945    2.867742471893    1.018638681364
              16 O      7.252555637507    2.449279690693   -0.173541677906
              17 O     -1.175512480130    2.067330391815   -1.203400850504
              18 O      0.627984376257    3.265049940109   -1.062402487865
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     8 
    Coordinates:
               0 C      0.008832948640   -0.093000745146   -0.093595054584
               1 C      0.705665668032    1.091803248044   -0.286731294677
               2 C      0.655730788967   -1.178714478007    0.479022629943
               3 C      2.056912368568    1.220472782468    0.084800017574
               4 C      2.692415400960    0.107271279519    0.662990858497
               5 C      1.994536390099   -1.074908263781    0.854535413284
               6 N      0.000289860390    2.223896908194   -0.895462095506
               7 C      2.807638147820    2.402168785111   -0.088671515057
               8 H     -1.030427456900   -0.150035118581   -0.394277071897
               9 H      0.113958399914   -2.106326933039    0.631464611537
              10 H      3.733070292669    0.196976893256    0.954575433990
              11 H      2.502381383689   -1.923490833849    1.302284208898
              12 N      3.612756025939    3.237400897844   -0.138462300976
              13 O      4.437656863270    4.124550507146   -0.191692534025
              14 O      5.549147954859    1.876392184225    1.564356024606
              15 S      6.494561663311    2.875756427730    1.020463896720
              16 O      7.257839522999    2.465853237751   -0.171388242046
              17 O     -1.175045087269    2.069287028971   -1.202236738356
              18 O      0.630569864043    3.264246192143   -1.063217247928
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     9 
    Coordinates:
               0 C      0.007982676083   -0.092878685792   -0.093383265811
               1 C      0.705368374622    1.091539388381   -0.286645293266
               2 C      0.654328884866   -1.179002547917    0.479103772710
               3 C      2.056701176813    1.219419876458    0.084677977058
               4 C      2.691683852609    0.105864415105    0.662850076746
               5 C      1.993223231612   -1.075955684494    0.854488141315
               6 N      0.000425614105    2.224150019450   -0.895314877563
               7 C      2.808615960224    2.400327772055   -0.088851951916
               8 H     -1.031358654387   -0.149328346538   -0.393907503535
               9 H      0.111990610958   -2.106290052498    0.631511373313
              10 H      3.732420068226    0.195068018742    0.954331372576
              11 H      2.500699017602   -1.924799397945    1.302158645104
              12 N      3.613025472491    3.236133234446   -0.139457228926
              13 O      4.438975435889    4.122280094534   -0.192921033827
              14 O      5.552643876766    1.876585760972    1.564062437775
              15 S      6.494967194173    2.879495174163    1.021171687865
              16 O      7.260481484176    2.472865511834   -0.170298638889
              17 O     -1.175080196003    2.070147649811   -1.201549977993
              18 O      0.631396919172    3.263977799233   -1.063266712737
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     10 
    Coordinates:
               0 C      0.007623587657   -0.092967220267   -0.093430596326
               1 C      0.705215829387    1.091336528875   -0.286559677331
               2 C      0.653751083014   -1.179225755834    0.479022031172
               3 C      2.056469513492    1.219099687687    0.084817800302
               4 C      2.691263903230    0.105376171262    0.663040667157
               5 C      1.992635380982   -1.076332816386    0.854539773406
               6 N      0.000319251624    2.224221423434   -0.895298804790
               7 C      2.808771270311    2.399774105935   -0.088673253457
               8 H     -1.031688677383   -0.148938659621   -0.394146714164
               9 H      0.111283484204   -2.106458644281    0.631290242798
              10 H      3.731996833804    0.194630473280    0.954515368960
              11 H      2.499984597039   -1.925265495433    1.302193526487
              12 N      3.612895395707    3.235822393501   -0.139599243882
              13 O      4.439418107799    4.121380158651   -0.193939960623
              14 O      5.554793755387    1.876436517504    1.564115104995
              15 S      6.495567606868    2.880884750563    1.021347721883
              16 O      7.261926682907    2.475565570227   -0.169942862672
              17 O     -1.175220262622    2.070382306210   -1.201327921742
              18 O      0.631483656594    3.263878504691   -1.063204202172
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     11 
    Coordinates:
               0 C      0.007623587657   -0.092967220267   -0.093430596326
               1 C      0.705215829387    1.091336528875   -0.286559677331
               2 C      0.653751083014   -1.179225755834    0.479022031172
               3 C      2.056469513492    1.219099687687    0.084817800302
               4 C      2.691263903230    0.105376171262    0.663040667157
               5 C      1.992635380982   -1.076332816386    0.854539773406
               6 N      0.000319251624    2.224221423434   -0.895298804790
               7 C      2.808771270311    2.399774105935   -0.088673253457
               8 H     -1.031688677383   -0.148938659621   -0.394146714164
               9 H      0.111283484204   -2.106458644281    0.631290242798
              10 H      3.731996833804    0.194630473280    0.954515368960
              11 H      2.499984597039   -1.925265495433    1.302193526487
              12 N      3.612895395707    3.235822393501   -0.139599243882
              13 O      4.439418107799    4.121380158651   -0.193939960623
              14 O      5.554793755387    1.876436517504    1.564115104995
              15 S      6.495567606868    2.880884750563    1.021347721883
              16 O      7.261926682907    2.475565570227   -0.169942862672
              17 O     -1.175220262622    2.070382306210   -1.201327921742
              18 O      0.631483656594    3.263878504691   -1.063204202172
