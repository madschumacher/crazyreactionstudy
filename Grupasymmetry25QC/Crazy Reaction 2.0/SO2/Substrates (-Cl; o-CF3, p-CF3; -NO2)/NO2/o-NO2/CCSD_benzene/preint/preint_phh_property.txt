-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1148.1499477032
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1153
     Surface Area:         860.4475812383
     Dielectric Energy:     -0.0130314890
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1417     6.0000    -0.1417     3.8351     3.8351    -0.0000
  1   0     5.9508     6.0000     0.0492     3.8409     3.8409     0.0000
  2   0     6.1441     6.0000    -0.1441     3.7781     3.7781    -0.0000
  3   0     5.9411     6.0000     0.0589     3.8742     3.8742     0.0000
  4   0     6.1083     6.0000    -0.1083     3.7715     3.7715     0.0000
  5   0     6.1159     6.0000    -0.1159     3.7839     3.7839     0.0000
  6   0     6.4871     7.0000     0.5129     4.1937     4.1937     0.0000
  7   0     5.9509     6.0000     0.0491     3.9641     3.9641    -0.0000
  8   0     0.7897     1.0000     0.2103     0.9767     0.9767     0.0000
  9   0     0.8107     1.0000     0.1893     0.9674     0.9674     0.0000
 10   0     0.8073     1.0000     0.1927     1.0007     1.0007     0.0000
 11   0     0.8101     1.0000     0.1899     0.9660     0.9660     0.0000
 12   0     6.6912     7.0000     0.3088     3.9754     3.9754    -0.0000
 13   0     8.4856     8.0000    -0.4856     1.6854     1.6854    -0.0000
 14   0     8.5148     8.0000    -0.5148     1.9997     1.9997    -0.0000
 15   0    15.0279    16.0000     0.9721     3.8548     3.8548     0.0000
 16   0     8.4862     8.0000    -0.4862     2.0214     2.0214     0.0000
 17   0     8.3648     8.0000    -0.3648     1.8892     1.8892     0.0000
 18   0     8.3719     8.0000    -0.3719     1.8688     1.8688    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.391681
                0             6               2            6                1.358545
                0             6               8            1                0.941391
                1             6               3            6                1.339652
                1             6               6            7                0.946226
                2             6               5            6                1.356863
                2             6               9            1                0.966249
                3             6               4            6                1.335073
                3             6               7            6                1.074628
                4             6               5            6                1.369786
                4             6              10            1                0.943429
                5             6              11            1                0.966673
                6             7              17            8                1.637943
                6             7              18            8                1.599793
                7             6              12            7                2.577248
                7             6              13            8                0.237431
               12             7              13            8                1.333416
               14             8              15           16                1.865575
               15            16              16            8                1.914309
               17             8              18            8                0.158621
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.1499477331
        Total Correlation Energy:                                          -2.9952398624
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1487698629
        Total MDCI Energy:                                              -1151.1451875955
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1148.2142192164
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1153
     Surface Area:         860.4475812383
     Dielectric Energy:     -0.0129602250
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 19
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 20
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1257     6.0000    -0.1257     3.8456     3.8456    -0.0000
  1   0     6.0129     6.0000    -0.0129     3.8398     3.8398    -0.0000
  2   0     6.1258     6.0000    -0.1258     3.8000     3.8000    -0.0000
  3   0     5.9403     6.0000     0.0597     3.8800     3.8800    -0.0000
  4   0     6.0974     6.0000    -0.0974     3.7527     3.7527    -0.0000
  5   0     6.0999     6.0000    -0.0999     3.7886     3.7886    -0.0000
  6   0     6.5179     7.0000     0.4821     4.2243     4.2243    -0.0000
  7   0     5.8049     6.0000     0.1951     3.8841     3.8841    -0.0000
  8   0     0.7752     1.0000     0.2248     0.9776     0.9776     0.0000
  9   0     0.8023     1.0000     0.1977     0.9669     0.9669    -0.0000
 10   0     0.8118     1.0000     0.1882     1.0149     1.0149     0.0000
 11   0     0.8048     1.0000     0.1952     0.9680     0.9680    -0.0000
 12   0     6.7905     7.0000     0.2095     3.9215     3.9215    -0.0000
 13   0     8.5026     8.0000    -0.5026     1.6974     1.6974    -0.0000
 14   0     8.5797     8.0000    -0.5797     1.9361     1.9361    -0.0000
 15   0    14.9174    16.0000     1.0826     3.7972     3.7972     0.0000
 16   0     8.5423     8.0000    -0.5423     1.9553     1.9553     0.0000
 17   0     8.3731     8.0000    -0.3731     1.8928     1.8928    -0.0000
 18   0     8.3756     8.0000    -0.3756     1.8779     1.8779     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.413639
                0             6               2            6                1.378702
                0             6               8            1                0.938319
                1             6               3            6                1.309232
                1             6               6            7                0.972256
                2             6               5            6                1.381417
                2             6               9            1                0.967820
                3             6               4            6                1.317766
                3             6               7            6                1.147319
                4             6               5            6                1.381749
                4             6              10            1                0.952529
                5             6              11            1                0.968946
                6             7              17            8                1.660454
                6             7              18            8                1.627027
                7             6              12            7                2.495708
                7             6              13            8                0.201118
               12             7              13            8                1.364280
               14             8              15           16                1.838717
               15            16              16            8                1.876899
               17             8              18            8                0.154649
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    116
        Number of correlated Electrons                                                78
        Number of alpha correlated Electrons                                          39
        Number of beta correlated Electrons                                           39
        Reference Energy:                                               -1148.2142192728
        Total Correlation Energy:                                          -3.1837393176
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1628036991
        Total MDCI Energy:                                              -1151.3979585904
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -1148.1499477331 
       SCF energy with basis  cc-pVQZ :  -1148.2142192728 
       CBS SCF Energy                    :  -1148.2335845399 
       Correlation energy with basis  cc-pVTZ :  -1148.1499477331 
       Correlation energy with basis  cc-pVQZ :  -1148.2142192728 
       CBS Correlation Energy            :     -3.3179300974 
       CBS Total Energy                  :  -1151.5515146373 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       19
     number of electrons:                   116
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             510
     number of aux C basis functions:       2226
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14554-kw6H/preint_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        8.0334584613
        Electronic Contribution:
                  0    
      0       5.233715
      1       7.807303
      2      -0.476987
        Nuclear Contribution:
                  0    
      0      -5.999531
      1     -10.521903
      2       1.902993
        Total Dipole moment:
                  0    
      0      -0.765816
      1      -2.714600
      2       1.426006
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     1 
    Coordinates:
               0 C      0.007624000000   -0.092967000000   -0.093431000000
               1 C      0.705216000000    1.091337000000   -0.286560000000
               2 C      0.653751000000   -1.179226000000    0.479022000000
               3 C      2.056470000000    1.219100000000    0.084818000000
               4 C      2.691264000000    0.105376000000    0.663041000000
               5 C      1.992635000000   -1.076333000000    0.854540000000
               6 N      0.000319000000    2.224221000000   -0.895299000000
               7 C      2.808771000000    2.399774000000   -0.088673000000
               8 H     -1.031689000000   -0.148939000000   -0.394147000000
               9 H      0.111283000000   -2.106459000000    0.631290000000
              10 H      3.731997000000    0.194630000000    0.954515000000
              11 H      2.499985000000   -1.925265000000    1.302194000000
              12 N      3.612895000000    3.235822000000   -0.139599000000
              13 O      4.439418000000    4.121380000000   -0.193940000000
              14 O      5.554794000000    1.876437000000    1.564115000000
              15 S      6.495568000000    2.880885000000    1.021348000000
              16 O      7.261927000000    2.475566000000   -0.169943000000
              17 O     -1.175220000000    2.070382000000   -1.201328000000
              18 O      0.631484000000    3.263879000000   -1.063204000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    19 
    Geometry Index:     2 
    Coordinates:
               0 C      0.007624000000   -0.092967000000   -0.093431000000
               1 C      0.705216000000    1.091337000000   -0.286560000000
               2 C      0.653751000000   -1.179226000000    0.479022000000
               3 C      2.056470000000    1.219100000000    0.084818000000
               4 C      2.691264000000    0.105376000000    0.663041000000
               5 C      1.992635000000   -1.076333000000    0.854540000000
               6 N      0.000319000000    2.224221000000   -0.895299000000
               7 C      2.808771000000    2.399774000000   -0.088673000000
               8 H     -1.031689000000   -0.148939000000   -0.394147000000
               9 H      0.111283000000   -2.106459000000    0.631290000000
              10 H      3.731997000000    0.194630000000    0.954515000000
              11 H      2.499985000000   -1.925265000000    1.302194000000
              12 N      3.612895000000    3.235822000000   -0.139599000000
              13 O      4.439418000000    4.121380000000   -0.193940000000
              14 O      5.554794000000    1.876437000000    1.564115000000
              15 S      6.495568000000    2.880885000000    1.021348000000
              16 O      7.261927000000    2.475566000000   -0.169943000000
              17 O     -1.175220000000    2.070382000000   -1.201328000000
              18 O      0.631484000000    3.263879000000   -1.063204000000
