-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -600.8609711183
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                943
     Surface Area:         677.0624709247
     Dielectric Energy:     -0.0122295285
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1389     6.0000    -0.1389     3.8346     3.8346    -0.0000
  1   0     5.9522     6.0000     0.0478     3.8287     3.8287    -0.0000
  2   0     6.1484     6.0000    -0.1484     3.7781     3.7781     0.0000
  3   0     5.9391     6.0000     0.0609     3.8697     3.8697    -0.0000
  4   0     6.1041     6.0000    -0.1041     3.7799     3.7799    -0.0000
  5   0     6.1184     6.0000    -0.1184     3.7862     3.7862    -0.0000
  6   0     6.4885     7.0000     0.5115     4.1969     4.1969    -0.0000
  7   0     5.9987     6.0000     0.0013     3.9192     3.9192     0.0000
  8   0     0.7894     1.0000     0.2106     0.9767     0.9767    -0.0000
  9   0     0.8112     1.0000     0.1888     0.9677     0.9677    -0.0000
 10   0     0.8120     1.0000     0.1880     0.9705     0.9705    -0.0000
 11   0     0.8117     1.0000     0.1883     0.9660     0.9660    -0.0000
 12   0     6.6984     7.0000     0.3016     4.0400     4.0400    -0.0000
 13   0     8.4509     8.0000    -0.4509     1.7617     1.7617    -0.0000
 14   0     8.3697     8.0000    -0.3697     1.8834     1.8834     0.0000
 15   0     8.3683     8.0000    -0.3683     1.8715     1.8715    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.384833
                0             6               2            6                1.365999
                0             6               8            1                0.941680
                1             6               3            6                1.345794
                1             6               6            7                0.947467
                2             6               5            6                1.350813
                2             6               9            1                0.966076
                3             6               4            6                1.307416
                3             6               7            6                1.082142
                4             6               5            6                1.379653
                4             6              10            1                0.965783
                5             6              11            1                0.966675
                6             7              14            8                1.632103
                6             7              15            8                1.606304
                7             6              12            7                2.535941
                7             6              13            8                0.267176
               12             7              13            8                1.442012
               14             8              15            8                0.158118
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.8609711435
        Total Correlation Energy:                                          -2.3046751064
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1161729440
        Total MDCI Energy:                                               -603.1656462498
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -600.9027696550
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                943
     Surface Area:         677.0624709247
     Dielectric Energy:     -0.0123277191
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 16
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1337     6.0000    -0.1337     3.8447     3.8447     0.0000
  1   0     6.0103     6.0000    -0.0103     3.8077     3.8077     0.0000
  2   0     6.1260     6.0000    -0.1260     3.8002     3.8002    -0.0000
  3   0     5.9654     6.0000     0.0346     3.9054     3.9054     0.0000
  4   0     6.0820     6.0000    -0.0820     3.7470     3.7470    -0.0000
  5   0     6.1053     6.0000    -0.1053     3.7943     3.7943    -0.0000
  6   0     6.5210     7.0000     0.4790     4.2305     4.2305     0.0000
  7   0     5.8898     6.0000     0.1102     3.8593     3.8593     0.0000
  8   0     0.7744     1.0000     0.2256     0.9777     0.9777     0.0000
  9   0     0.8029     1.0000     0.1971     0.9673     0.9673    -0.0000
 10   0     0.8103     1.0000     0.1897     0.9772     0.9772    -0.0000
 11   0     0.8057     1.0000     0.1943     0.9682     0.9682     0.0000
 12   0     6.7483     7.0000     0.2517     3.9713     3.9713     0.0000
 13   0     8.4751     8.0000    -0.4751     1.7606     1.7606     0.0000
 14   0     8.3782     8.0000    -0.3782     1.8863     1.8863     0.0000
 15   0     8.3714     8.0000    -0.3714     1.8824     1.8824    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.405539
                0             6               2            6                1.383677
                0             6               8            1                0.940353
                1             6               3            6                1.319018
                1             6               6            7                0.974674
                2             6               5            6                1.374285
                2             6               9            1                0.967652
                3             6               4            6                1.274857
                3             6               7            6                1.175206
                4             6               5            6                1.391781
                4             6              10            1                0.976878
                5             6              11            1                0.970121
                6             7              14            8                1.654350
                6             7              15            8                1.635137
                7             6              12            7                2.444522
                7             6              13            8                0.232252
               12             7              13            8                1.459851
               14             8              15            8                0.153686
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     84
        Number of correlated Electrons                                                60
        Number of alpha correlated Electrons                                          30
        Number of beta correlated Electrons                                           30
        Reference Energy:                                                -600.9027698364
        Total Correlation Energy:                                          -2.4416334586
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1260638409
        Total MDCI Energy:                                               -603.3444032950
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -600.8609711435 
       SCF energy with basis  cc-pVQZ :   -600.9027698364 
       CBS SCF Energy                    :   -600.9153639466 
       Correlation energy with basis  cc-pVTZ :   -600.8609711435 
       Correlation energy with basis  cc-pVQZ :   -600.9027698364 
       CBS Correlation Energy            :     -2.5391326692 
       CBS Total Energy                  :   -603.4544966158 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       16
     number of electrons:                   84
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             416
     number of aux C basis functions:       1804
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14556-hING/reagent_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        9.5530987732
        Electronic Contribution:
                  0    
      0       0.823452
      1       6.108373
      2       0.014031
        Nuclear Contribution:
                  0    
      0      -1.666247
      1      -9.770938
      2      -0.044035
        Total Dipole moment:
                  0    
      0      -0.842795
      1      -3.662565
      2      -0.030004
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.068647000000   -0.216217000000    0.355439000000
               1 C      0.757279000000    0.899587000000    0.415757000000
               2 C      0.488426000000   -1.484462000000    0.414608000000
               3 C      2.155657000000    0.778033000000    0.535046000000
               4 C      2.695006000000   -0.521409000000    0.592026000000
               5 C      1.870596000000   -1.632536000000    0.532823000000
               6 N      0.132765000000    2.224867000000    0.351441000000
               7 C      3.035088000000    1.878065000000    0.596838000000
               8 H     -1.138859000000   -0.075866000000    0.263536000000
               9 H     -0.155062000000   -2.356812000000    0.368554000000
              10 H      3.769999000000   -0.633496000000    0.683156000000
              11 H      2.310237000000   -2.624176000000    0.579065000000
              12 N      3.995319000000    2.536433000000    0.663406000000
              13 O      4.932486000000    3.288850000000    0.731273000000
              14 O     -1.085875000000    2.277324000000    0.227272000000
              15 O      0.863173000000    3.208344000000    0.425370000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.068647000000   -0.216217000000    0.355439000000
               1 C      0.757279000000    0.899587000000    0.415757000000
               2 C      0.488426000000   -1.484462000000    0.414608000000
               3 C      2.155657000000    0.778033000000    0.535046000000
               4 C      2.695006000000   -0.521409000000    0.592026000000
               5 C      1.870596000000   -1.632536000000    0.532823000000
               6 N      0.132765000000    2.224867000000    0.351441000000
               7 C      3.035088000000    1.878065000000    0.596838000000
               8 H     -1.138859000000   -0.075866000000    0.263536000000
               9 H     -0.155062000000   -2.356812000000    0.368554000000
              10 H      3.769999000000   -0.633496000000    0.683156000000
              11 H      2.310237000000   -2.624176000000    0.579065000000
              12 N      3.995319000000    2.536433000000    0.663406000000
              13 O      4.932486000000    3.288850000000    0.731273000000
              14 O     -1.085875000000    2.277324000000    0.227272000000
              15 O      0.863173000000    3.208344000000    0.425370000000
