-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -397.3231539053
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 15
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1288     6.0000    -0.1288     3.7902     3.7902     0.0000
  1   0     6.1577     6.0000    -0.1577     3.8414     3.8414     0.0000
  2   0     6.1496     6.0000    -0.1496     3.8322     3.8322     0.0000
  3   0     5.9117     6.0000     0.0883     3.7573     3.7573     0.0000
  4   0     6.1568     6.0000    -0.1568     3.8411     3.8411     0.0000
  5   0     6.1289     6.0000    -0.1289     3.7899     3.7899     0.0000
  6   0     0.8333     1.0000     0.1667     0.9798     0.9798     0.0000
  7   0     6.0759     6.0000    -0.0759     3.8930     3.8930     0.0000
  8   0     0.8383     1.0000     0.1617     0.9752     0.9752    -0.0000
  9   0     0.8374     1.0000     0.1626     0.9770     0.9770     0.0000
 10   0     0.8332     1.0000     0.1668     0.9797     0.9797    -0.0000
 11   0     0.8383     1.0000     0.1617     0.9752     0.9752    -0.0000
 12   0     6.6798     7.0000     0.3202     4.0523     4.0523    -0.0000
 13   0     8.4303     8.0000    -0.4303     1.7783     1.7783    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.390113
                0             6               2            6                1.369620
                0             6               8            1                0.974212
                1             6               3            6                1.323751
                1             6               6            1                0.972796
                2             6               5            6                1.369125
                2             6               9            1                0.975474
                3             6               4            6                1.322850
                3             6               7            6                1.071400
                4             6               5            6                1.390253
                4             6              10            1                0.972828
                5             6              11            1                0.974167
                7             6              12            7                2.539835
                7             6              13            8                0.278164
               12             7              13            8                1.455761
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     62
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -397.3231539074
        Total Correlation Energy:                                          -1.6163486685
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0814064748
        Total MDCI Energy:                                               -398.9395025759
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -397.3495199812
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 15
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0987     6.0000    -0.0987     3.7874     3.7874    -0.0000
  1   0     6.1636     6.0000    -0.1636     3.8556     3.8556     0.0000
  2   0     6.1478     6.0000    -0.1478     3.8539     3.8539    -0.0000
  3   0     5.9663     6.0000     0.0337     3.8011     3.8011     0.0000
  4   0     6.1614     6.0000    -0.1614     3.8559     3.8559     0.0000
  5   0     6.0994     6.0000    -0.0994     3.7874     3.7874     0.0000
  6   0     0.8372     1.0000     0.1628     0.9901     0.9901    -0.0000
  7   0     5.9643     6.0000     0.0357     3.8111     3.8111     0.0000
  8   0     0.8455     1.0000     0.1545     0.9853     0.9853     0.0000
  9   0     0.8385     1.0000     0.1615     0.9858     0.9858    -0.0000
 10   0     0.8369     1.0000     0.1631     0.9899     0.9899     0.0000
 11   0     0.8454     1.0000     0.1546     0.9852     0.9852    -0.0000
 12   0     6.7465     7.0000     0.2535     3.9915     3.9915     0.0000
 13   0     8.4485     8.0000    -0.4485     1.7867     1.7867    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.408884
                0             6               2            6                1.382587
                0             6               8            1                0.985771
                1             6               3            6                1.309824
                1             6               6            1                0.984231
                2             6               5            6                1.381971
                2             6               9            1                0.985729
                3             6               4            6                1.309015
                3             6               7            6                1.163691
                4             6               5            6                1.408760
                4             6              10            1                0.984359
                5             6              11            1                0.985682
                7             6              12            7                2.446991
                7             6              13            8                0.231808
               12             7              13            8                1.489772
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     62
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -397.3495199829
        Total Correlation Energy:                                          -1.7053199842
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0879047476
        Total MDCI Energy:                                               -399.0548399671
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -397.3231539074 
       SCF energy with basis  cc-pVQZ :   -397.3495199829 
       CBS SCF Energy                    :   -397.3574641849 
       Correlation energy with basis  cc-pVTZ :   -397.3231539074 
       Correlation energy with basis  cc-pVQZ :   -397.3495199829 
       CBS Correlation Energy            :     -1.7686577282 
       CBS Total Energy                  :   -399.1261219131 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       14
     number of electrons:                   62
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             340
     number of aux C basis functions:       1463
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14385-8CH7/cc_reagent.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        6.0017010515
        Electronic Contribution:
                  0    
      0       4.125919
      1       5.120060
      2       0.299023
        Nuclear Contribution:
                  0    
      0      -5.614824
      1      -6.949910
      2      -0.399476
        Total Dipole moment:
                  0    
      0      -1.488905
      1      -1.829850
      2      -0.100453
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.069210000000   -0.065296000000    0.353512000000
               1 C      0.791195000000    1.020773000000    0.417689000000
               2 C      0.429899000000   -1.364572000000    0.407188000000
               3 C      2.175757000000    0.811022000000    0.537192000000
               4 C      2.675335000000   -0.501768000000    0.591475000000
               5 C      1.801408000000   -1.576849000000    0.526092000000
               6 H      0.407658000000    2.034876000000    0.376569000000
               7 C      3.061915000000    1.913794000000    0.602642000000
               8 H     -1.137826000000    0.103841000000    0.261021000000
               9 H     -0.248893000000   -2.210375000000    0.356466000000
              10 H      3.744529000000   -0.661291000000    0.684203000000
              11 H      2.194048000000   -2.588361000000    0.568126000000
              12 N      3.802599000000    2.811967000000    0.648429000000
              13 O      4.567886000000    3.740538000000    0.695396000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.069210000000   -0.065296000000    0.353512000000
               1 C      0.791195000000    1.020773000000    0.417689000000
               2 C      0.429899000000   -1.364572000000    0.407188000000
               3 C      2.175757000000    0.811022000000    0.537192000000
               4 C      2.675335000000   -0.501768000000    0.591475000000
               5 C      1.801408000000   -1.576849000000    0.526092000000
               6 H      0.407658000000    2.034876000000    0.376569000000
               7 C      3.061915000000    1.913794000000    0.602642000000
               8 H     -1.137826000000    0.103841000000    0.261021000000
               9 H     -0.248893000000   -2.210375000000    0.356466000000
              10 H      3.744529000000   -0.661291000000    0.684203000000
              11 H      2.194048000000   -2.588361000000    0.568126000000
              12 N      3.802599000000    2.811967000000    0.648429000000
              13 O      4.567886000000    3.740538000000    0.695396000000
