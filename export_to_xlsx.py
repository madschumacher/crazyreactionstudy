from pathlib import Path
from orca_inout_parser_from_outfile import parse_output_energies
import os
import json
import csv


def directory_to_nested_dict(directory_path):
    if not os.path.isdir(directory_path):
        raise ValueError("The provided path is not a directory.")
    nested_dict = {}
    for entry in os.scandir(directory_path):
        path = str(entry.path).casefold()
        if "neb" in path or "irc" in path or "mtd" in path or ".git" in path:
            pass
        elif entry.is_dir():
            print(entry.path)
            nested_dict[entry.name] = directory_to_nested_dict(entry.path)
        else:
            if entry.name.split(".")[-1] == "out" or entry.name.split(".")[-1] == "log":
               nested_dict[entry.name] =  parse_output_energies(entry.path)

    return nested_dict


def format_calc_data(data, path):
    calc_data = {
        "path": path,
        **data
    }

    return calc_data


def get_calculations(data: dict, path: str):
    calcs = []

    for key, value in data.items():
        current_path = path + "." + key
        if key.endswith("out") or key.endswith(".log"):
            calcs.append(format_calc_data(value, current_path))
        else:
            calcs.extend(get_calculations(value, current_path))

    return calcs


def convert_to_excell(json_path, csv_path):
    with open(json_path, "r") as f:
        data = json.load(f)

    rows = get_calculations(data, "")

    with open(csv_path, "w") as csv_file:
        fieldname_set = set()
        for row in rows:
            fieldname_set = fieldname_set.union(set(row.keys()))

        fieldnames = list(fieldname_set)
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerows(rows)



if __name__ == "__main__":

    # Provide the directory path to convert to a nested dictionary
    target_directory = Path("/home/olgert/projects/quantum_chemichal_nedopyramid/crazyreactionstudy/")
    try:
        nested_dict = directory_to_nested_dict(target_directory)
    except ValueError as e:
        print(f"Error: {e}")


    with open(target_directory / "crazy_report.json", "w") as f:
        json.dump(nested_dict, f)


    convert_to_excell(target_directory / "crazy_report.json", target_directory / "crazy_report.csv")
