-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -946.9375251279
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                966
     Surface Area:         700.1134384266
     Dielectric Energy:     -0.0080304516
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0159     6.0000    -0.0159     3.8532     3.8532     0.0000
  1   0     5.9844     6.0000     0.0156     3.7733     3.7733     0.0000
  2   0     5.9865     6.0000     0.0135     3.8630     3.8630    -0.0000
  3   0     5.8772     6.0000     0.1228     3.6493     3.6493     0.0000
  4   0     5.9884     6.0000     0.0116     3.7834     3.7834     0.0000
  5   0     6.0159     6.0000    -0.0159     3.8560     3.8560    -0.0000
  6   0     0.9803     1.0000     0.0197     1.0116     1.0116     0.0000
  7   0     5.6861     6.0000     0.3139     3.8935     3.8935     0.0000
  8   0     0.9770     1.0000     0.0230     0.9978     0.9978    -0.0000
  9   0     0.9758     1.0000     0.0242     0.9960     0.9960     0.0000
 10   0     0.9868     1.0000     0.0132     1.0130     1.0130     0.0000
 11   0     0.9779     1.0000     0.0221     0.9976     0.9976     0.0000
 12   0     7.1763     7.0000    -0.1763     2.8131     2.8131     0.0000
 13   0     8.4150     8.0000    -0.4150     1.8270     1.8270    -0.0000
 14   0     8.4811     8.0000    -0.4811     1.9613     1.9613    -0.0000
 15   0    14.9497    16.0000     1.0503     3.7194     3.7194     0.0000
 16   0     8.5259     8.0000    -0.5259     1.9319     1.9319    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.413251
                0             6               2            6                1.365949
                0             6               8            1                0.999193
                1             6               3            6                1.303666
                1             6               6            1                0.997262
                2             6               5            6                1.393682
                2             6               9            1                0.997656
                3             6               4            6                1.329489
                3             6               7            6                0.973633
                4             6               5            6                1.387333
                4             6              10            1                0.998492
                5             6              11            1                0.999243
                7             6              12            7                1.847021
                7             6              14            8                1.050545
               12             7              13            8                0.835528
               13             8              15           16                0.915031
               14             8              15           16                0.853391
               15            16              16            8                1.870170
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -946.9375251313
        Total Correlation Energy:                                          -2.4471420376
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1304341319
        Total MDCI Energy:                                               -949.3846671689
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             863
     number of aux C basis functions:       1998
     number of aux J basis functions:       8822
     number of aux JK basis functions:      8822
     number of aux CABS basis functions:    0
     Total Energy                           -949.384667
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : i.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.7491551111
        Electronic Contribution:
                  0    
      0      10.229492
      1       7.837684
      2      -0.423486
        Nuclear Contribution:
                  0    
      0     -11.179016
      1      -8.821868
      2       1.696590
        Total Dipole moment:
                  0    
      0      -0.949524
      1      -0.984183
      2       1.273104
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.075660064216    0.090774046806   -0.086153012372
               1 C      1.059115030239    1.057724674392   -0.227323970698
               2 C      0.386815122589   -1.157029989412    0.454978446802
               3 C      2.373304603668    0.774234624075    0.170620818880
               4 C      2.686658947362   -0.476541744586    0.713893403449
               5 C      1.690406033352   -1.436010249124    0.854063114297
               6 H      0.819647074473    2.031914374968   -0.641051682243
               7 C      3.412263387333    1.788118495844    0.021576770303
               8 H     -0.941474081210    0.311592732262   -0.395716198295
               9 H     -0.388680925924   -1.909336382588    0.565738499690
              10 H      3.703910866390   -0.696607505609    1.019372385565
              11 H      1.936513029566   -2.406070762754    1.275265133156
              12 N      3.265556764937    2.893563115725   -0.617569243501
              13 O      4.453277495973    3.643785466836   -0.525433459480
              14 O      4.625519266446    1.542830261746    0.610442390173
              15 S      5.782798425184    2.676806359127    0.021887272241
              16 O      6.433107895407    2.067250482292   -1.143890667967
