-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -397.7732511888
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                837
     Surface Area:         575.3156073085
     Dielectric Energy:     -0.0047471938
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 14
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 15
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0621     6.0000    -0.0621     3.9286     3.9286    -0.0000
  1   0     6.2026     6.0000    -0.2026     4.0177     4.0177    -0.0000
  2   0     6.1557     6.0000    -0.1557     3.9909     3.9909    -0.0000
  3   0     5.7009     6.0000     0.2991     3.7375     3.7375    -0.0000
  4   0     6.2208     6.0000    -0.2208     4.0117     4.0117    -0.0000
  5   0     6.0762     6.0000    -0.0762     3.9317     3.9317     0.0000
  6   0     0.8736     1.0000     0.1264     1.0098     1.0098    -0.0000
  7   0     5.4327     6.0000     0.5673     4.2081     4.2081    -0.0000
  8   0     0.8738     1.0000     0.1262     0.9956     0.9956    -0.0000
  9   0     0.8749     1.0000     0.1251     0.9973     0.9973    -0.0000
 10   0     0.8821     1.0000     0.1179     1.0037     1.0037    -0.0000
 11   0     0.8719     1.0000     0.1281     0.9959     0.9959     0.0000
 12   0     7.3963     7.0000    -0.3963     3.1589     3.1589    -0.0000
 13   0     8.3764     8.0000    -0.3764     2.1889     2.1889    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.476507
                0             6               2            6                1.449997
                0             6               8            1                0.976078
                1             6               3            6                1.420752
                1             6               6            1                0.973170
                2             6               5            6                1.444026
                2             6               9            1                0.978051
                3             6               4            6                1.388805
                3             6              12            7                0.950396
                4             6               5            6                1.475401
                4             6              10            1                0.975498
                5             6              11            1                0.971393
                7             6              12            7                2.106019
                7             6              13            8                2.042584
               12             7              13            8                0.115372
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     62
        Number of correlated Electrons                                                44
        Number of alpha correlated Electrons                                          22
        Number of beta correlated Electrons                                           22
        Reference Energy:                                                -397.7732511793
        Total Correlation Energy:                                          -1.5967573540
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.0810993966
        Total MDCI Energy:                                               -399.3700085333
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       14
     number of electrons:                   62
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             358
     number of aux C basis functions:       834
     number of aux J basis functions:       3907
     number of aux JK basis functions:      3907
     number of aux CABS basis functions:    0
     Total Energy                           -399.370009
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : iv.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.0116305894
        Electronic Contribution:
                  0    
      0       6.267998
      1       3.296954
      2      -1.631296
        Nuclear Contribution:
                  0    
      0      -7.266959
      1      -3.873186
      2       1.903115
        Total Dipole moment:
                  0    
      0      -0.998961
      1      -0.576231
      2       0.271819
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C      1.803763931256   -0.430017115856    0.365803865283
               1 C      2.885355971852    0.376359151178    0.029076433682
               2 C      1.991773985997   -1.778658437986    0.657172360641
               3 C      4.166947175085   -0.174357364385   -0.016014669509
               4 C      4.364767286786   -1.528053925216    0.275498496143
               5 C      3.274686269135   -2.321524197774    0.610340867461
               6 H      2.750294606596    1.428662736579   -0.199899385977
               7 C      6.427049822336    0.558767690725   -0.498303671341
               8 H      0.807577171769    0.001366403485    0.399944914835
               9 H      1.144290024852   -2.404527246615    0.919457615618
              10 H      5.366218025871   -1.948274289657    0.237790512952
              11 H      3.430754860993   -3.372545486547    0.836072870323
              12 N      5.233062271781    0.665820450994   -0.359236738567
              13 O      7.587058595690    0.597581631075   -0.666705471545
