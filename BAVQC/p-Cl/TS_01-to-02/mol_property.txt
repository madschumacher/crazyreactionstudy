-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1407.7197224981
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 55.0000490455 
   Number of Beta  Electrons                 55.0000490455 
   Total number of  Electrons               110.0000980910 
   Exchange energy                         -121.8426083375 
   Correlation energy                        -3.8619026737 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7045110112 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7197224981 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0074874961
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1407.7178028433
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 55.0000646697 
   Number of Beta  Electrons                 55.0000646697 
   Total number of  Electrons               110.0001293395 
   Exchange energy                         -121.8316710112 
   Correlation energy                        -3.8600513266 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.6917223377 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7178028433 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0074931694
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1407.7183991531
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 55.0000486772 
   Number of Beta  Electrons                 55.0000486772 
   Total number of  Electrons               110.0000973544 
   Exchange energy                         -121.8384649335 
   Correlation energy                        -3.8582247737 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.6966897072 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7183991531 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0074672291
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1407.7192525673
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 55.0000413965 
   Number of Beta  Electrons                 55.0000413965 
   Total number of  Electrons               110.0000827929 
   Exchange energy                         -121.8359494167 
   Correlation energy                        -3.8597013136 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.6956507302 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7192525673 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0074633719
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1407.7195784547
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 55.0000426025 
   Number of Beta  Electrons                 55.0000426025 
   Total number of  Electrons               110.0000852050 
   Exchange energy                         -121.8418878679 
   Correlation energy                        -3.8611518966 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7030397645 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7195784547 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0074695693
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1407.7219325621
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 55.0000455305 
   Number of Beta  Electrons                 55.0000455305 
   Total number of  Electrons               110.0000910609 
   Exchange energy                         -121.8561999575 
   Correlation energy                        -3.8658403526 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7220403101 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7219325621 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0075057339
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:    -1407.7196759202
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 55.0000444583 
   Number of Beta  Electrons                 55.0000444583 
   Total number of  Electrons               110.0000889165 
   Exchange energy                         -121.8410694692 
   Correlation energy                        -3.8617740981 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7028435674 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7196759202 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0074886057
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:    -1407.7196976804
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 55.0000445909 
   Number of Beta  Electrons                 55.0000445909 
   Total number of  Electrons               110.0000891819 
   Exchange energy                         -121.8419645503 
   Correlation energy                        -3.8617856072 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7037501575 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7196976804 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0074868534
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:    -1407.7197009777
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 55.0000461086 
   Number of Beta  Electrons                 55.0000461086 
   Total number of  Electrons               110.0000922172 
   Exchange energy                         -121.8424368361 
   Correlation energy                        -3.8618251679 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7042620040 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7197009777 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0074928738
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 10
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 10
   prop. index: 1
        SCF Energy:    -1407.7197033921
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 10
   prop. index: 1
   Number of Alpha Electrons                 55.0000448162 
   Number of Beta  Electrons                 55.0000448162 
   Total number of  Electrons               110.0000896325 
   Exchange energy                         -121.8420765381 
   Correlation energy                        -3.8618186960 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7038952341 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7197033921 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 10
   prop. index: 1
        Van der Waals Correction:       -0.0074895267
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 11
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 11
   prop. index: 1
        SCF Energy:    -1407.7197040170
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 11
   prop. index: 1
   Number of Alpha Electrons                 55.0000447148 
   Number of Beta  Electrons                 55.0000447148 
   Total number of  Electrons               110.0000894297 
   Exchange energy                         -121.8418948510 
   Correlation energy                        -3.8617493144 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7036441654 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7197040170 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 11
   prop. index: 1
        Van der Waals Correction:       -0.0074873723
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 12
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 12
   prop. index: 1
        SCF Energy:    -1407.7197095358
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 12
   prop. index: 1
   Number of Alpha Electrons                 55.0000447348 
   Number of Beta  Electrons                 55.0000447348 
   Total number of  Electrons               110.0000894696 
   Exchange energy                         -121.8421918013 
   Correlation energy                        -3.8617795966 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7039713979 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7197095358 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 12
   prop. index: 1
        Van der Waals Correction:       -0.0074880577
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 12
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.3675412707
        Electronic Contribution:
                  0    
      0       1.107773
      1      -0.062211
      2       0.251150
        Nuclear Contribution:
                  0    
      0      -2.159399
      1      -0.545003
      2       0.278608
        Total Dipole moment:
                  0    
      0      -1.051626
      1      -0.607214
      2       0.529758
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 13
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 13
   prop. index: 1
        SCF Energy:    -1407.7197095294
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 13
   prop. index: 1
   Number of Alpha Electrons                 55.0000447345 
   Number of Beta  Electrons                 55.0000447345 
   Total number of  Electrons               110.0000894691 
   Exchange energy                         -121.8421902335 
   Correlation energy                        -3.8617798263 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7039700598 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7197095294 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 13
   prop. index: 1
        Van der Waals Correction:       -0.0074880577
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 13
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        216.9600418485
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1407.7113866232
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0088262536
        Number of frequencies          :     51      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     -300.128823
      7      18.335939
      8      43.610299
      9      80.560259
     10      94.051189
     11     160.168505
     12     212.305256
     13     220.403297
     14     287.437379
     15     312.777160
     16     355.496271
     17     392.911253
     18     405.653825
     19     452.300614
     20     490.294667
     21     499.430947
     22     508.643882
     23     554.237515
     24     627.209941
     25     650.355735
     26     697.729681
     27     783.854813
     28     826.345805
     29     835.161267
     30     955.650670
     31     971.629469
     32     983.241846
     33     1035.781975
     34     1098.998832
     35     1126.042776
     36     1137.882855
     37     1206.754587
     38     1219.038791
     39     1320.478211
     40     1325.006178
     41     1376.412623
     42     1430.846901
     43     1511.989943
     44     1584.674437
     45     1619.711789
     46     2019.104305
     47     3191.706667
     48     3200.473321
     49     3205.462406
     50     3215.287585
        Zero Point Energy (Hartree)    :          0.1007985557
        Inner Energy (Hartree)         :      -1407.5989292711
        Enthalpy (Hartree)             :      -1407.5979850621
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0153836864
        Vibrational entropy            :          0.0156654279
        Translational entropy          :          0.0153836864
        Entropy                        :          0.0510173196
        Gibbs Energy (Hartree)         :      -1407.6490023817
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.277402685537   -0.794912496227   -0.638204886045
               1 C     -1.412202056159    0.285238687812   -0.623580544769
               2 C     -1.923972478198   -1.960818946457    0.036982208147
               3 C     -0.200751293401    0.205777986075    0.085098849692
               4 C      0.139345642429   -0.975854254829    0.758190489921
               5 C     -0.721338975227   -2.061211874190    0.729416444878
               6 H     -1.668406829715    1.196956939658   -1.152818970344
               7 C      0.675329034072    1.322180700967    0.109955743340
               8 H     -3.216881426353   -0.739758982795   -1.177020507188
               9 Cl    -3.010801649990   -3.326057609258    0.005548703492
              10 H      1.081817934774   -1.035406012662    1.289385768583
              11 H     -0.464021568814   -2.980955207470    1.242839776229
              12 N      0.834323302329    2.503611507353   -0.056983521000
              13 O      1.749950362547    3.348536941292   -0.079850298259
              14 O      2.586223128988    0.852580121910    0.639433862589
              15 S      3.549815180160    1.930854127409    0.156645921835
              16 O      3.983364378094    1.779078371413   -1.243929041102
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.271110094441   -0.789126201871   -0.643832331369
               1 C     -1.408789708655    0.293314263551   -0.626610482612
               2 C     -1.910853155313   -1.958702636941    0.021359432352
               3 C     -0.192908227458    0.212034550588    0.074265202855
               4 C      0.153798235949   -0.972983973905    0.737908441378
               5 C     -0.704323659644   -2.060318693996    0.706839214069
               6 H     -1.670387280414    1.207953564536   -1.148110831635
               7 C      0.680351816059    1.330642717777    0.102299888458
               8 H     -3.213775759581   -0.732802548863   -1.176946679871
               9 Cl    -2.994405804932   -3.326328597172   -0.012510371864
              10 H      1.099378465253   -1.033515341892    1.263425895293
              11 H     -0.441812239364   -2.982626227727    1.212984548761
              12 N      0.834989252741    2.513969163522   -0.055157908986
              13 O      1.749070078216    3.360612968387   -0.078421478230
              14 O      2.662386035043    0.975790499744    0.885764028291
              15 S      3.553948135064    1.941625227108    0.113243322293
              16 O      3.778833911480    1.570301267153   -1.295389889184
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     3 
    Coordinates:
               0 C     -2.221589871904   -0.809175257236   -0.716460816540
               1 C     -1.370472867437    0.280560637564   -0.665264657713
               2 C     -1.899855230108   -1.962252793679   -0.002547728148
               3 C     -0.222070053212    0.233010842157    0.146552226582
               4 C      0.105120277976   -0.946471382145    0.830048686760
               5 C     -0.740759720930   -2.041492499816    0.763618583194
               6 H     -1.603008794286    1.182830433049   -1.220588669254
               7 C      0.650506225886    1.343504720858    0.208015097539
               8 H     -3.128228036759   -0.770316120877   -1.310276716319
               9 Cl    -2.967690474332   -3.339532506192   -0.083972295949
              10 H      1.023486720336   -0.988900711975    1.406078969636
              11 H     -0.501238114603   -2.954596689167    1.297079897447
              12 N      0.859390706016    2.520667364921    0.107428578299
              13 O      1.754232040983    3.388141775879    0.094303502378
              14 O      2.832424542995    0.912640341355    0.776029365445
              15 S      3.583006079073    1.912917535872   -0.056043824792
              16 O      3.551136570306    1.588304309429   -1.492890198567
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     4 
    Coordinates:
               0 C     -2.240425745500   -0.808177247992   -0.689660711077
               1 C     -1.380793539754    0.275628217980   -0.648062437032
               2 C     -1.915858742397   -1.964138354517    0.016870906925
               3 C     -0.212208864521    0.214864884188    0.131583387889
               4 C      0.107597591704   -0.962347353767    0.822382420851
               5 C     -0.746923914610   -2.051428316119    0.766645118773
               6 H     -1.614820131584    1.179904893500   -1.199750520394
               7 C      0.663396990394    1.329100955687    0.180654272415
               8 H     -3.154139585973   -0.763105581401   -1.272023634121
               9 Cl    -2.995000321119   -3.333782926845   -0.048933903972
              10 H      1.028556388273   -1.012807228610    1.391479488902
              11 H     -0.508550476187   -2.965308910958    1.299324616496
              12 N      0.838859967566    2.511017214179    0.052223697720
              13 O      1.746248088779    3.363973594067    0.037760327981
              14 O      2.705093956180    0.846071919652    0.638314714116
              15 S      3.580185792874    1.924839207689    0.025334788308
              16 O      3.803172545874    1.765535033267   -1.423032533783
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     5 
    Coordinates:
               0 C     -2.243747696919   -0.809605484698   -0.683692308844
               1 C     -1.379583610246    0.270933392403   -0.646813905797
               2 C     -1.922182482905   -1.964331985147    0.025844973284
               3 C     -0.207643326377    0.206778129826    0.126849150467
               4 C      0.105163383185   -0.966755819258    0.826664946242
               5 C     -0.752723693489   -2.053244323234    0.774817551532
               6 H     -1.610201206113    1.173859024479   -1.202172819353
               7 C      0.671329738259    1.319507153873    0.172795714032
               8 H     -3.157356100968   -0.763668386439   -1.266163656814
               9 Cl    -3.006629523436   -3.329980045547   -0.032953381939
              10 H      1.025552188453   -1.018794452182    1.396658242200
              11 H     -0.517676044782   -2.965788665762    1.311266262665
              12 N      0.840455821784    2.503994131185    0.054830745256
              13 O      1.749090892647    3.354084353511    0.032304034308
              14 O      2.643313945215    0.816151924230    0.533371368211
              15 S      3.571271874084    1.919483813293    0.047002297149
              16 O      3.895955841607    1.857217239467   -1.389499212599
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     6 
    Coordinates:
               0 C     -2.269056287488   -0.785586453564   -0.618390272696
               1 C     -1.396488490912    0.289378241268   -0.614990047746
               2 C     -1.912767268450   -1.953157578101    0.051288657062
               3 C     -0.168897436845    0.198013704025    0.061189658619
               4 C      0.166454569562   -0.979035801049    0.742473157627
               5 C     -0.700789995303   -2.059428741287    0.726630874063
               6 H     -1.653841467208    1.202099389489   -1.142217468679
               7 C      0.705475710985    1.320606495336    0.080225731074
               8 H     -3.214493901234   -0.725592411819   -1.146049957911
               9 Cl    -3.005230780776   -3.314256368038    0.033558592120
              10 H      1.109456213727   -1.042951765977    1.272317976561
              11 H     -0.444888829813   -2.977502636687    1.243719627746
              12 N      0.805466601295    2.513683108980   -0.102713623679
              13 O      1.748996562331    3.331047248594   -0.113181627900
              14 O      2.414193694450    0.834158095605    0.551687401629
              15 S      3.476037379549    1.901140197841    0.205306449511
              16 O      4.044763726129    1.797225275384   -1.149745127401
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     7 
    Coordinates:
               0 C     -2.253031716012   -0.804179810488   -0.667225626594
               1 C     -1.388070864872    0.276159198027   -0.640852478391
               2 C     -1.919150006315   -1.961853966914    0.031598746280
               3 C     -0.198633458371    0.205953072199    0.104536064703
               4 C      0.122049577448   -0.967502047662    0.800825538557
               5 C     -0.736888225378   -2.053685315414    0.759851819370
               6 H     -1.627936210039    1.181183879966   -1.188894670778
               7 C      0.678435461910    1.321872333224    0.142913103119
               8 H     -3.176365031298   -0.756028790039   -1.233938335103
               9 Cl    -3.004732464701   -3.327205061789   -0.013154899215
              10 H      1.049527537267   -1.020976302428    1.358845180565
              11 H     -0.494296250889   -2.967200931437    1.291261836586
              12 N      0.828978767340    2.510529352930    0.018676114932
              13 O      1.746509990881    3.352622820704   -0.005973784050
              14 O      2.598867604378    0.820338379263    0.564693769603
              15 S      3.552091629991    1.914348056595    0.090349564142
              16 O      3.927033658659    1.825465133261   -1.332401943727
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     8 
    Coordinates:
               0 C     -2.250269647619   -0.804957102394   -0.671200961739
               1 C     -1.385174964213    0.275185086567   -0.642850617968
               2 C     -1.919309624270   -1.962042263721    0.030052648804
               3 C     -0.198699363348    0.205353831879    0.107373825915
               4 C      0.119395129698   -0.967719195934    0.805706541142
               5 C     -0.739707558758   -2.053637988672    0.762754231148
               6 H     -1.622766349579    1.179786524808   -1.192569356806
               7 C      0.678208383363    1.320677114873    0.149234470447
               8 H     -3.171583449329   -0.757107527385   -1.241219263716
               9 Cl    -3.004833366368   -3.327255530505   -0.017930365608
              10 H      1.044773346245   -1.020353559929    1.367255603720
              11 H     -0.499434175689   -2.966672055178    1.296031227770
              12 N      0.833278740681    2.508576579828    0.027245783830
              13 O      1.749890792000    3.352260239049    0.003932546115
              14 O      2.607087551350    0.824252985101    0.564688630414
              15 S      3.554302746616    1.916632758572    0.080236249074
              16 O      3.909231809221    1.826860103043   -1.347631192540
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     9 
    Coordinates:
               0 C     -2.248545178556   -0.804143873602   -0.671750083396
               1 C     -1.384512173529    0.276844204729   -0.643238138794
               2 C     -1.916366534823   -1.961169165885    0.029015195638
               3 C     -0.197200230468    0.207223408750    0.105579959518
               4 C      0.121436172236   -0.965243896703    0.804798220894
               5 C     -0.736638617309   -2.051917854552    0.761700392039
               6 H     -1.622868653181    1.181356779400   -1.192756010336
               7 C      0.677122858050    1.324273272202    0.150761140016
               8 H     -3.170012911916   -0.757023963273   -1.241579926033
               9 Cl    -3.000010567541   -3.327782450772   -0.020288144829
              10 H      1.046261276189   -1.016299736950    1.367445123502
              11 H     -0.495987925292   -2.964558263481    1.295478431505
              12 N      0.832136637092    2.511671601018    0.025837869818
              13 O      1.752840776828    3.351221639818    0.007062134807
              14 O      2.606220878114    0.819746754511    0.575053786200
              15 S      3.547981002488    1.911730189652    0.078978370772
              16 O      3.892533191617    1.813911355139   -1.350988321320
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     10 
    Coordinates:
               0 C     -2.250193367673   -0.804720292108   -0.670716617589
               1 C     -1.385531310155    0.275793666119   -0.642860869331
               2 C     -1.918320538966   -1.961680729083    0.030287797591
               3 C     -0.198318081892    0.206195419818    0.106116781516
               4 C      0.120359264902   -0.966504767083    0.804843016469
               5 C     -0.738264593861   -2.052802166586    0.762353290938
               6 H     -1.623735275761    1.180282580421   -1.192498721093
               7 C      0.677961017738    1.322070147275    0.147935685825
               8 H     -3.171817517817   -0.757305522972   -1.240267050091
               9 Cl    -3.003259726008   -3.327363284578   -0.017107547221
              10 H      1.045909640010   -1.018570358664    1.366163030958
              11 H     -0.497441502395   -2.965664072443    1.295676612485
              12 N      0.831506327435    2.510296150095    0.026970279862
              13 O      1.750467718893    3.351481527357    0.003601561641
              14 O      2.603915073189    0.819179501600    0.563331265446
              15 S      3.551268939723    1.913562194399    0.082189635776
              16 O      3.909883932637    1.825590006432   -1.344908153182
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     11 
    Coordinates:
               0 C     -2.249473487503   -0.805441035414   -0.672460563203
               1 C     -1.384802241458    0.275050431648   -0.643894320002
               2 C     -1.918835600093   -1.961953978782    0.029850100816
               3 C     -0.199024318252    0.205914160333    0.107373881558
               4 C      0.118477979280   -0.966412871883    0.807379337477
               5 C     -0.740030184734   -2.052721040199    0.764045567311
               6 H     -1.621900539668    1.179154761324   -1.194634038620
               7 C      0.677359726825    1.321510621640    0.150278749867
               8 H     -3.170136468300   -0.758396773157   -1.243597212574
               9 Cl    -3.003758361617   -3.327567290894   -0.018502456135
              10 H      1.043228193251   -1.018016667811    1.370061375436
              11 H     -0.500162348403   -2.965304556771    1.298275442261
              12 N      0.832580246519    2.509690103201    0.032002457620
              13 O      1.752155855903    3.350622317331    0.007945176120
              14 O      2.609016780787    0.818561962192    0.559619073345
              15 S      3.553372802678    1.914679874277    0.078076190168
              16 O      3.906321964786    1.830469982964   -1.350708761443
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     12 
    Coordinates:
               0 C     -2.248827344609   -0.805673546720   -0.673248744066
               1 C     -1.384450069025    0.275049809238   -0.644337353608
               2 C     -1.918392084831   -1.961913763774    0.029601973270
               3 C     -0.199272723996    0.206394326812    0.107903633247
               4 C      0.118059681002   -0.965705017517    0.808429442034
               5 C     -0.740069945679   -2.052258682513    0.764674446599
               6 H     -1.621311853015    1.178948634945   -1.195514938679
               7 C      0.676960881552    1.321988076016    0.151239649113
               8 H     -3.169087894962   -0.759041532061   -1.245067986971
               9 Cl    -3.002909724365   -3.327800109117   -0.019206414225
              10 H      1.042478621744   -1.016831981271    1.371730341932
              11 H     -0.500373984069   -2.964682000980    1.299251109376
              12 N      0.832990595568    2.510153241398    0.034700540633
              13 O      1.753406986036    3.350213403409    0.010018524123
              14 O      2.608460283181    0.816781506949    0.557342905152
              15 S      3.552675062708    1.913101894219    0.076456318156
              16 O      3.904053512763    1.831115740967   -1.352863446085
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     13 
    Coordinates:
               0 C     -2.248827344609   -0.805673546720   -0.673248744066
               1 C     -1.384450069025    0.275049809238   -0.644337353608
               2 C     -1.918392084831   -1.961913763774    0.029601973270
               3 C     -0.199272723996    0.206394326812    0.107903633247
               4 C      0.118059681002   -0.965705017517    0.808429442034
               5 C     -0.740069945679   -2.052258682513    0.764674446599
               6 H     -1.621311853015    1.178948634945   -1.195514938679
               7 C      0.676960881552    1.321988076016    0.151239649113
               8 H     -3.169087894962   -0.759041532061   -1.245067986971
               9 Cl    -3.002909724365   -3.327800109117   -0.019206414225
              10 H      1.042478621744   -1.016831981271    1.371730341932
              11 H     -0.500373984069   -2.964682000980    1.299251109376
              12 N      0.832990595568    2.510153241398    0.034700540633
              13 O      1.753406986036    3.350213403409    0.010018524123
              14 O      2.608460283181    0.816781506949    0.557342905152
              15 S      3.552675062708    1.913101894219    0.076456318156
              16 O      3.904053512763    1.831115740967   -1.352863446085
