-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -859.2348439423
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 38.9999935111 
   Number of Beta  Electrons                 38.9999935111 
   Total number of  Electrons                77.9999870222 
   Exchange energy                          -80.3377670486 
   Correlation energy                        -2.6711379810 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -83.0089050296 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.2348439423 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0053453640
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -859.2348438062
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 38.9999935131 
   Number of Beta  Electrons                 38.9999935131 
   Total number of  Electrons                77.9999870261 
   Exchange energy                          -80.3376891341 
   Correlation energy                        -2.6711343062 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -83.0088234403 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.2348438062 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0053453189
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.5597887517
        Electronic Contribution:
                  0    
      0      -1.415732
      1      -1.601318
      2       0.570513
        Nuclear Contribution:
                  0    
      0       1.198690
      1       1.586475
      2      -0.536237
        Total Dipole moment:
                  0    
      0      -0.217043
      1      -0.014843
      2       0.034276
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -859.2348438064
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 38.9999935131 
   Number of Beta  Electrons                 38.9999935131 
   Total number of  Electrons                77.9999870261 
   Exchange energy                          -80.3377022568 
   Correlation energy                        -2.6711346786 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -83.0088369354 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -859.2348438064 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0053453189
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 3
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        152.9981414349
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -859.2343278455
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0054617799
        Number of frequencies          :     42      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      49.914424
      7      87.371177
      8     135.764204
      9     266.769950
     10     330.434214
     11     348.366033
     12     417.830868
     13     431.105677
     14     496.061526
     15     567.373116
     16     575.523980
     17     648.207832
     18     680.237616
     19     699.206762
     20     803.205978
     21     817.462400
     22     836.048378
     23     940.270343
     24     960.550294
     25     1037.975959
     26     1100.534079
     27     1129.841377
     28     1150.220552
     29     1201.253855
     30     1318.582047
     31     1330.165767
     32     1435.358639
     33     1471.772242
     34     1539.875427
     35     1603.697841
     36     1629.892057
     37     2349.309928
     38     3175.429775
     39     3192.384675
     40     3200.647853
     41     3205.917572
        Zero Point Energy (Hartree)    :          0.0937797781
        Inner Energy (Hartree)         :       -859.1322537446
        Enthalpy (Hartree)             :       -859.1313095356
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0143061652
        Vibrational entropy            :          0.0092046571
        Translational entropy          :          0.0143061652
        Entropy                        :          0.0429843418
        Gibbs Energy (Hartree)         :       -859.1742938773
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     1 
    Coordinates:
               0 C      1.806706147216   -0.418493417892    0.362676720334
               1 C      2.891784570415    0.379631398480    0.027025840280
               2 C      2.011790216050   -1.762569362585    0.650845719266
               3 C      4.175363381636   -0.165491656152   -0.019812871391
               4 C      4.367979169955   -1.519358689957    0.272402913934
               5 C      3.285667685142   -2.319652024647    0.608187035703
               6 H      2.751999523125    1.430944005203   -0.200990580137
               7 C      6.432126136012    0.558177327400   -0.497294142985
               8 H      0.807173704134    0.000521706309    0.400517357569
               9 Cl     0.645411394842   -2.772258102666    1.074926217382
              10 H      5.366945488324   -1.944653522165    0.236288533469
              11 H      3.429434637027   -3.370347519490    0.835653013558
              12 N      5.236556924366    0.675152448216   -0.362653072745
              13 O      7.590701021755    0.584237409946   -0.663462684237
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     2 
    Coordinates:
               0 C      1.806690781619   -0.418500893032    0.362680981779
               1 C      2.891784399325    0.379615989915    0.027023178950
               2 C      2.011788093758   -1.762578130771    0.650848099351
               3 C      4.175396957492   -0.165468769985   -0.019833124720
               4 C      4.367969077687   -1.519351348832    0.272393766328
               5 C      3.285651750281   -2.319664678645    0.608187888975
               6 H      2.751924940668    1.430925377473   -0.200979884179
               7 C      6.432170690666    0.558210684643   -0.497327736317
               8 H      0.807170827418    0.000538056490    0.400522803370
               9 Cl     0.645387513637   -2.772247002521    1.074940773226
              10 H      5.366899686118   -1.944744265673    0.236309851562
              11 H      3.429448008327   -3.370354510725    0.835653102618
              12 N      5.236586658106    0.675180331844   -0.362660902259
              13 O      7.590770614898    0.584279159817   -0.663448798685
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    14 
    Geometry Index:     3 
    Coordinates:
               0 C      1.806690781619   -0.418500893032    0.362680981779
               1 C      2.891784399325    0.379615989915    0.027023178950
               2 C      2.011788093758   -1.762578130771    0.650848099351
               3 C      4.175396957492   -0.165468769985   -0.019833124720
               4 C      4.367969077687   -1.519351348832    0.272393766328
               5 C      3.285651750281   -2.319664678645    0.608187888975
               6 H      2.751924940668    1.430925377473   -0.200979884179
               7 C      6.432170690666    0.558210684643   -0.497327736317
               8 H      0.807170827418    0.000538056490    0.400522803370
               9 Cl     0.645387513637   -2.772247002521    1.074940773226
              10 H      5.366899686118   -1.944744265673    0.236309851562
              11 H      3.429448008327   -3.370354510725    0.835653102618
              12 N      5.236586658106    0.675180331844   -0.362660902259
              13 O      7.590770614898    0.584279159817   -0.663448798685
