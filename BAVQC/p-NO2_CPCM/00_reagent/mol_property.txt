-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -604.0674090165
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 41.9999928047 
   Number of Beta  Electrons                 41.9999928047 
   Total number of  Electrons                83.9999856093 
   Exchange energy                          -75.8915244949 
   Correlation energy                        -2.8113859794 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7029104744 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0674090165 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                948
     Surface Area:         691.8576452726
     Dielectric Energy:     -0.0072745194
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0059426432
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -604.0674865512
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 41.9999934636 
   Number of Beta  Electrons                 41.9999934636 
   Total number of  Electrons                83.9999869271 
   Exchange energy                          -75.8900557842 
   Correlation energy                        -2.8111744007 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7012301849 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0674865512 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9899438006
     Dielectric Energy:     -0.0073892607
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0059428570
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -604.0675089305
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 41.9999938258 
   Number of Beta  Electrons                 41.9999938258 
   Total number of  Electrons                83.9999876517 
   Exchange energy                          -75.8891932163 
   Correlation energy                        -2.8110825478 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7002757641 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0675089305 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9964008983
     Dielectric Energy:     -0.0074563334
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0059436181
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -604.0674770329
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 41.9999942538 
   Number of Beta  Electrons                 41.9999942538 
   Total number of  Electrons                83.9999885076 
   Exchange energy                          -75.8886842695 
   Correlation energy                        -2.8110360709 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.6997203404 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0674770329 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                946
     Surface Area:         691.8644596601
     Dielectric Energy:     -0.0074967168
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0059441641
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -604.0675107662
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 41.9999938315 
   Number of Beta  Electrons                 41.9999938315 
   Total number of  Electrons                83.9999876630 
   Exchange energy                          -75.8891010071 
   Correlation energy                        -2.8110795601 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7001805672 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0675107662 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9590434683
     Dielectric Energy:     -0.0074627049
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0059437338
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -604.0675100870
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 41.9999937799 
   Number of Beta  Electrons                 41.9999937799 
   Total number of  Electrons                83.9999875598 
   Exchange energy                          -75.8891484636 
   Correlation energy                        -2.8110833454 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7002318090 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0675100870 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9460997442
     Dielectric Energy:     -0.0074619589
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0059437100
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 6
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.4059274697
        Electronic Contribution:
                  0    
      0       0.722337
      1       0.871982
      2       0.048362
        Nuclear Contribution:
                  0    
      0      -0.376144
      1      -0.441676
      2      -0.017826
        Total Dipole moment:
                  0    
      0       0.346193
      1       0.430306
      2       0.030537
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -604.0675100830
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 41.9999937800 
   Number of Beta  Electrons                 41.9999937800 
   Total number of  Electrons                83.9999875601 
   Exchange energy                          -75.8891453371 
   Correlation energy                        -2.8110832546 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -78.7002285916 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -604.0675100830 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                947
     Surface Area:         691.9460997442
     Dielectric Energy:     -0.0074619247
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0059437100
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        164.0221919965
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -604.0624239526
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0071037870
        Number of frequencies          :     48      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      31.359736
      7      49.100832
      8      52.068636
      9     145.189979
     10     240.614717
     11     306.778504
     12     311.040083
     13     409.671108
     14     415.039811
     15     462.826917
     16     482.334452
     17     484.542599
     18     539.714515
     19     542.670858
     20     648.546633
     21     687.017565
     22     757.077961
     23     758.861479
     24     835.633181
     25     862.959787
     26     868.593364
     27     977.196226
     28     985.286670
     29     1033.481876
     30     1108.280678
     31     1131.787614
     32     1133.382652
     33     1203.775281
     34     1316.068107
     35     1342.620509
     36     1352.723743
     37     1432.192226
     38     1447.136003
     39     1527.766663
     40     1544.817133
     41     1619.197666
     42     1625.361737
     43     2420.870293
     44     3201.594632
     45     3201.870608
     46     3222.187974
     47     3222.973352
        Zero Point Energy (Hartree)    :          0.1046640656
        Inner Energy (Hartree)         :       -603.9478235572
        Enthalpy (Hartree)             :       -603.9468793481
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0146124844
        Vibrational entropy            :          0.0123185904
        Translational entropy          :          0.0146124844
        Entropy                        :          0.0465031327
        Gibbs Energy (Hartree)         :       -603.9933824808
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.117464285037   -0.068087229380    0.349149730025
               1 C      0.756954126829    1.001796372208    0.414088694366
               2 C      0.394500371963   -1.359569345335    0.404335930200
               3 C      2.141371940952    0.776807513464    0.533948106474
               4 C      2.631718758322   -0.541714835518    0.587972096267
               5 C      1.756797595515   -1.611120452197    0.522703954438
               6 H      0.382491812678    2.018740445243    0.373174478097
               7 C      3.034342134924    1.868711364324    0.600154992654
               8 H     -1.186544697748    0.081875278576    0.256625384157
               9 N     -0.538014515929   -2.500179867816    0.335234741003
              10 H      3.698768089049   -0.711658257255    0.680833924336
              11 H      2.112142375281   -2.633952798379    0.561903145296
              12 N      3.781189268287    2.762594501319    0.647051951534
              13 O      4.547440415429    3.683941066204    0.696327050880
              14 O     -1.732110726904   -2.247733814210    0.234066145806
              15 O     -0.057512663613   -3.625609941248    0.383689674468
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.117964880629   -0.067044850350    0.349231581386
               1 C      0.756371248863    1.002482541820    0.414185644812
               2 C      0.395050401488   -1.358844028971    0.404571844803
               3 C      2.140476620018    0.775629891576    0.534081816521
               4 C      2.632513023099   -0.542217502887    0.588382502175
               5 C      1.757855407913   -1.611419390623    0.523104487526
               6 H      0.381811696434    2.019527365262    0.373090653277
               7 C      3.033538779912    1.867479659162    0.600002113520
               8 H     -1.186654954920    0.085403762271    0.256364266909
               9 N     -0.536629491952   -2.498532142838    0.335022779310
              10 H      3.699644470469   -0.712202740537    0.681268908660
              11 H      2.115509115989   -2.633425933006    0.562544139643
              12 N      3.779320415362    2.760298703282    0.646630654133
              13 O      4.547218287248    3.683965458261    0.695893690166
              14 O     -1.732390901452   -2.249975865459    0.234845667090
              15 O     -0.059599237842   -3.626284926961    0.382039250070
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     3 
    Coordinates:
               0 C     -0.118587228659   -0.066870149825    0.349520086147
               1 C      0.755850540261    1.002648478045    0.414224206486
               2 C      0.395193172182   -1.358578826834    0.405155785684
               3 C      2.139598810320    0.774383096632    0.534100659248
               4 C      2.632808470759   -0.542866504521    0.588811965628
               5 C      1.758045959363   -1.612053652242    0.523697860271
               6 H      0.381182171694    2.019586701915    0.372909594281
               7 C      3.032928635789    1.866361254845    0.599686827795
               8 H     -1.186902967923    0.087859214412    0.256584220310
               9 N     -0.535198622382   -2.496678479814    0.335775570840
              10 H      3.699849759839   -0.712997244327    0.681758775586
              11 H      2.117743792907   -2.633283885247    0.563545816752
              12 N      3.778237719506    2.759029124354    0.646068121005
              13 O      4.546815985482    3.684091560691    0.695271518846
              14 O     -1.731991295076   -2.249913462663    0.234911595835
              15 O     -0.059504904063   -3.625877225418    0.379237395286
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     4 
    Coordinates:
               0 C     -0.118748993642   -0.066955394711    0.346648659748
               1 C      0.755705403560    1.002670360279    0.412811183372
               2 C      0.395455938886   -1.358625802763    0.401524103958
               3 C      2.139065978930    0.773446204008    0.533577939321
               4 C      2.633055107899   -0.543373953953    0.587756752118
               5 C      1.758218452631   -1.612614062973    0.521080435745
               6 H      0.380854513680    2.019496716702    0.371955471181
               7 C      3.032520941873    1.865583993003    0.600712421728
               8 H     -1.186643930445    0.089764905944    0.252980502474
               9 N     -0.533780563004   -2.495460505741    0.330165505040
              10 H      3.699936223153   -0.713710274850    0.681458625743
              11 H      2.119715422595   -2.633153427553    0.560563594325
              12 N      3.777363008661    2.758402318165    0.648425461521
              13 O      4.545926587459    3.684326648049    0.699176817257
              14 O     -1.732522534234   -2.249519554447    0.245465537347
              15 O     -0.060051558003   -3.625438169160    0.386956989123
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     5 
    Coordinates:
               0 C     -0.118626615610   -0.067046708147    0.349068628804
               1 C      0.755907706133    1.002590279929    0.413915021641
               2 C      0.395088288698   -1.358766754613    0.404664995976
               3 C      2.139496878270    0.774067508209    0.533980261279
               4 C      2.632829647690   -0.543035703931    0.588708798323
               5 C      1.757903750888   -1.612280741770    0.523378671465
               6 H      0.381137977832    2.019455005225    0.372576666390
               7 C      3.032956621507    1.866140905496    0.599778662659
               8 H     -1.186741824412    0.088647277129    0.256041425062
               9 N     -0.534946684448   -2.496361361227    0.335123695876
              10 H      3.699798650541   -0.713326041000    0.681832571317
              11 H      2.118426257616   -2.633162514520    0.563294313439
              12 N      3.778099173832    2.759059627154    0.646386134859
              13 O      4.546351699988    3.684277003410    0.695904417072
              14 O     -1.732148979482   -2.249626359745    0.236973536225
              15 O     -0.059462549042   -3.625791421599    0.379632199611
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     6 
    Coordinates:
               0 C     -0.118576449451   -0.067117129302    0.349050698974
               1 C      0.756001226809    1.002548904413    0.413845409117
               2 C      0.395003483377   -1.358874290903    0.404689070942
               3 C      2.139553648414    0.774004541253    0.533959716503
               4 C      2.632821025366   -0.543092303209    0.588759270321
               5 C      1.757813401270   -1.612330853109    0.523430339830
               6 H      0.381172877830    2.019381014454    0.372421212317
               7 C      3.033067414128    1.866093283535    0.599737404206
               8 H     -1.186629566877    0.088958392737    0.256005583865
               9 N     -0.535007743605   -2.496368380878    0.335171290923
              10 H      3.699760632228   -0.713498513988    0.681930868601
              11 H      2.118636745124   -2.633099126093    0.563410224300
              12 N      3.778077420518    2.759162180912    0.646366198071
              13 O      4.546085219243    3.684437284174    0.695955769396
              14 O     -1.732239943451   -2.249553613976    0.237442844361
              15 O     -0.059469390922   -3.625811390021    0.379084098272
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    16 
    Geometry Index:     7 
    Coordinates:
               0 C     -0.118576449451   -0.067117129302    0.349050698974
               1 C      0.756001226809    1.002548904413    0.413845409117
               2 C      0.395003483377   -1.358874290903    0.404689070942
               3 C      2.139553648414    0.774004541253    0.533959716503
               4 C      2.632821025366   -0.543092303209    0.588759270321
               5 C      1.757813401270   -1.612330853109    0.523430339830
               6 H      0.381172877830    2.019381014454    0.372421212317
               7 C      3.033067414128    1.866093283535    0.599737404206
               8 H     -1.186629566877    0.088958392737    0.256005583865
               9 N     -0.535007743605   -2.496368380878    0.335171290923
              10 H      3.699760632228   -0.713498513988    0.681930868601
              11 H      2.118636745124   -2.633099126093    0.563410224300
              12 N      3.778077420518    2.759162180912    0.646366198071
              13 O      4.546085219243    3.684437284174    0.695955769396
              14 O     -1.732239943451   -2.249553613976    0.237442844361
              15 O     -0.059469390922   -3.625811390021    0.379084098272
