-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -944.5656193060
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1344     6.0000    -0.1344     3.7891     3.7891    -0.0000
  1   0     6.1411     6.0000    -0.1411     3.8561     3.8561    -0.0000
  2   0     6.1413     6.0000    -0.1413     3.8286     3.8286    -0.0000
  3   0     5.9176     6.0000     0.0824     3.7614     3.7614    -0.0000
  4   0     6.1477     6.0000    -0.1477     3.8431     3.8431    -0.0000
  5   0     6.1344     6.0000    -0.1344     3.7843     3.7843    -0.0000
  6   0     0.8316     1.0000     0.1684     0.9836     0.9836    -0.0000
  7   0     5.8364     6.0000     0.1636     3.8226     3.8226     0.0000
  8   0     0.8370     1.0000     0.1630     0.9752     0.9752    -0.0000
  9   0     0.8356     1.0000     0.1644     0.9763     0.9763    -0.0000
 10   0     0.8196     1.0000     0.1804     0.9963     0.9963    -0.0000
 11   0     0.8364     1.0000     0.1636     0.9753     0.9753     0.0000
 12   0     6.8270     7.0000     0.1730     3.6284     3.6284     0.0000
 13   0     8.4550     8.0000    -0.4550     1.7769     1.7769    -0.0000
 14   0     8.5512     8.0000    -0.5512     1.9380     1.9380     0.0000
 15   0    15.0619    16.0000     0.9381     3.8737     3.8737     0.0000
 16   0     8.4918     8.0000    -0.4918     2.0166     2.0166     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.396986
                0             6               2            6                1.361825
                0             6               8            1                0.973862
                1             6               3            6                1.306534
                1             6               6            1                0.969992
                2             6               5            6                1.374261
                2             6               9            1                0.975256
                3             6               4            6                1.343208
                3             6               7            6                1.063859
                4             6               5            6                1.378935
                4             6              10            1                0.954948
                5             6              11            1                0.975012
                7             6              12            7                2.300287
                7             6              13            8                0.173741
                7             6              14            8                0.221645
               12             7              13            8                1.219657
               13             8              15           16                0.306572
               14             8              15           16                1.614924
               15            16              16            8                1.893888
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.5656193062
        Total Correlation Energy:                                          -2.3299027225
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1199457156
        Total MDCI Energy:                                               -946.8955220286
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -944.6139907359
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1011     6.0000    -0.1011     3.7848     3.7848    -0.0000
  1   0     6.1337     6.0000    -0.1337     3.8818     3.8818    -0.0000
  2   0     6.1426     6.0000    -0.1426     3.8494     3.8494    -0.0000
  3   0     6.0088     6.0000    -0.0088     3.7738     3.7738     0.0000
  4   0     6.1582     6.0000    -0.1582     3.8390     3.8390     0.0000
  5   0     6.0938     6.0000    -0.0938     3.7756     3.7756    -0.0000
  6   0     0.8387     1.0000     0.1613     0.9970     0.9970     0.0000
  7   0     5.6992     6.0000     0.3008     3.7841     3.7841     0.0000
  8   0     0.8439     1.0000     0.1561     0.9853     0.9853    -0.0000
  9   0     0.8358     1.0000     0.1642     0.9842     0.9842    -0.0000
 10   0     0.8238     1.0000     0.1762     1.0093     1.0093     0.0000
 11   0     0.8439     1.0000     0.1561     0.9853     0.9853     0.0000
 12   0     6.9093     7.0000     0.0907     3.5820     3.5820     0.0000
 13   0     8.4420     8.0000    -0.4420     1.8057     1.8057    -0.0000
 14   0     8.5962     8.0000    -0.5962     1.8931     1.8931    -0.0000
 15   0    14.9787    16.0000     1.0213     3.8636     3.8636     0.0000
 16   0     8.5502     8.0000    -0.5502     1.9582     1.9582     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.416938
                0             6               2            6                1.373871
                0             6               8            1                0.985556
                1             6               3            6                1.289133
                1             6               6            1                0.986096
                2             6               5            6                1.386629
                2             6               9            1                0.984856
                3             6               4            6                1.313879
                3             6               7            6                1.152307
                4             6               5            6                1.399433
                4             6              10            1                0.964777
                5             6              11            1                0.985900
                7             6              12            7                2.225759
                7             6              13            8                0.151369
                7             6              14            8                0.238949
               12             7              13            8                1.239283
               13             8              15           16                0.331816
               14             8              15           16                1.602310
               15            16              16            8                1.867499
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                     94
        Number of correlated Electrons                                                62
        Number of alpha correlated Electrons                                          31
        Number of beta correlated Electrons                                           31
        Reference Energy:                                                -944.6139907278
        Total Correlation Energy:                                          -2.4711202125
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1308040647
        Total MDCI Energy:                                               -947.0851109403
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :   -944.5656193062 
       SCF energy with basis  cc-pVQZ :   -944.6139907278 
       CBS SCF Energy                    :   -944.6285652269 
       Correlation energy with basis  cc-pVTZ :   -944.5656193062 
       Correlation energy with basis  cc-pVQZ :   -944.6139907278 
       CBS Correlation Energy            :     -2.5716514585 
       CBS Total Energy                  :   -947.2002166854 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   94
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             434
     number of aux C basis functions:       1885
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14387-pxa1/cc_ts1.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.7350888182
        Electronic Contribution:
                  0    
      0       9.321014
      1       8.335838
      2       0.526323
        Nuclear Contribution:
                  0    
      0     -11.116424
      1      -9.547979
      2       0.104674
        Total Dipole moment:
                  0    
      0      -1.795411
      1      -1.212141
      2       0.630997
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.412483000000   -0.743353000000   -0.358515000000
               1 C     -1.541814000000    0.326389000000   -0.492368000000
               2 C     -1.962370000000   -1.958609000000    0.154071000000
               3 C     -0.193869000000    0.169107000000   -0.125463000000
               4 C      0.260877000000   -1.052165000000    0.391117000000
               5 C     -0.629900000000   -2.106483000000    0.530034000000
               6 H     -1.884208000000    1.279249000000   -0.882437000000
               7 C      0.707384000000    1.256600000000   -0.276040000000
               8 H     -3.451978000000   -0.625180000000   -0.648891000000
               9 H     -2.652737000000   -2.789516000000    0.262611000000
              10 H      1.300722000000   -1.149793000000    0.680886000000
              11 H     -0.279340000000   -3.051814000000    0.932451000000
              12 N      0.989253000000    2.293962000000   -0.816539000000
              13 O      1.854938000000    3.190330000000   -0.769989000000
              14 O      2.234904000000    1.217785000000    1.076953000000
              15 S      3.350598000000    2.163475000000    0.649248000000
              16 O      4.310024000000    1.580017000000   -0.307126000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.412483000000   -0.743353000000   -0.358515000000
               1 C     -1.541814000000    0.326389000000   -0.492368000000
               2 C     -1.962370000000   -1.958609000000    0.154071000000
               3 C     -0.193869000000    0.169107000000   -0.125463000000
               4 C      0.260877000000   -1.052165000000    0.391117000000
               5 C     -0.629900000000   -2.106483000000    0.530034000000
               6 H     -1.884208000000    1.279249000000   -0.882437000000
               7 C      0.707384000000    1.256600000000   -0.276040000000
               8 H     -3.451978000000   -0.625180000000   -0.648891000000
               9 H     -2.652737000000   -2.789516000000    0.262611000000
              10 H      1.300722000000   -1.149793000000    0.680886000000
              11 H     -0.279340000000   -3.051814000000    0.932451000000
              12 N      0.989253000000    2.293962000000   -0.816539000000
              13 O      1.854938000000    3.190330000000   -0.769989000000
              14 O      2.234904000000    1.217785000000    1.076953000000
              15 S      3.350598000000    2.163475000000    0.649248000000
              16 O      4.310024000000    1.580017000000   -0.307126000000
