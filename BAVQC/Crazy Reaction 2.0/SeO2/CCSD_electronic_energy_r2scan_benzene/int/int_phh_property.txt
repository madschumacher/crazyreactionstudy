-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2946.9703649534
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                967
     Surface Area:         722.5172180862
     Dielectric Energy:     -0.0109232655
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1580     6.0000    -0.1580     3.7725     3.7725    -0.0000
  1   0     6.1515     6.0000    -0.1515     3.8261     3.8261    -0.0000
  2   0     6.1516     6.0000    -0.1516     3.8010     3.8010     0.0000
  3   0     5.9493     6.0000     0.0507     3.7236     3.7236    -0.0000
  4   0     6.1657     6.0000    -0.1657     3.8341     3.8341    -0.0000
  5   0     6.1580     6.0000    -0.1580     3.7736     3.7736     0.0000
  6   0     0.8216     1.0000     0.1784     0.9912     0.9912     0.0000
  7   0     5.7055     6.0000     0.2945     4.0888     4.0888    -0.0000
  8   0     0.8262     1.0000     0.1738     0.9704     0.9704    -0.0000
  9   0     0.8243     1.0000     0.1757     0.9706     0.9706     0.0000
 10   0     0.8195     1.0000     0.1805     0.9854     0.9854    -0.0000
 11   0     0.8272     1.0000     0.1728     0.9711     0.9711     0.0000
 12   0     7.1134     7.0000    -0.1134     2.9144     2.9144    -0.0000
 13   0     8.4096     8.0000    -0.4096     1.8880     1.8880    -0.0000
 14   0     8.4493     8.0000    -0.4493     2.0341     2.0341     0.0000
 15   0    32.8580    34.0000     1.1420     3.7234     3.7234    -0.0000
 16   0     8.6114     8.0000    -0.6114     1.9117     1.9117    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.396253
                0             6               2            6                1.347394
                0             6               8            1                0.973090
                1             6               3            6                1.316796
                1             6               6            1                0.960823
                2             6               5            6                1.379497
                2             6               9            1                0.972077
                3             6               4            6                1.349487
                3             6               7            6                1.056428
                4             6               5            6                1.365182
                4             6              10            1                0.960695
                5             6              11            1                0.973470
                7             6              12            7                1.903867
                7             6              14            8                1.079916
               12             7              13            8                0.872492
               13             8              15           34                0.927551
               14             8              15           34                0.890183
               15            34              16            8                1.830850
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.9703648436
        Total Correlation Energy:                                          -2.3517023024
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1151541493
        Total MDCI Energy:                                              -2949.3220671460
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2947.0141446117
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                967
     Surface Area:         722.5172180862
     Dielectric Energy:     -0.0109342780
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1342     6.0000    -0.1342     3.7858     3.7858     0.0000
  1   0     6.1387     6.0000    -0.1387     3.7868     3.7868     0.0000
  2   0     6.1530     6.0000    -0.1530     3.8171     3.8171    -0.0000
  3   0     6.0504     6.0000    -0.0504     3.7586     3.7586     0.0000
  4   0     6.1379     6.0000    -0.1379     3.7793     3.7793     0.0000
  5   0     6.1343     6.0000    -0.1343     3.7891     3.7891    -0.0000
  6   0     0.8177     1.0000     0.1823     0.9993     0.9993     0.0000
  7   0     5.6395     6.0000     0.3605     4.0948     4.0948    -0.0000
  8   0     0.8251     1.0000     0.1749     0.9745     0.9745    -0.0000
  9   0     0.8187     1.0000     0.1813     0.9750     0.9750    -0.0000
 10   0     0.8225     1.0000     0.1775     1.0000     1.0000     0.0000
 11   0     0.8270     1.0000     0.1730     0.9758     0.9758    -0.0000
 12   0     7.1483     7.0000    -0.1483     2.9265     2.9265    -0.0000
 13   0     8.4323     8.0000    -0.4323     1.8808     1.8808     0.0000
 14   0     8.5009     8.0000    -0.5009     1.9868     1.9868     0.0000
 15   0    32.7650    34.0000     1.2350     3.6903     3.6903    -0.0000
 16   0     8.6544     8.0000    -0.6544     1.8871     1.8871     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.414397
                0             6               2            6                1.368939
                0             6               8            1                0.977568
                1             6               3            6                1.286590
                1             6               6            1                0.968511
                2             6               5            6                1.401243
                2             6               9            1                0.978993
                3             6               4            6                1.309817
                3             6               7            6                1.141437
                4             6               5            6                1.385152
                4             6              10            1                0.971904
                5             6              11            1                0.978547
                7             6              12            7                1.901896
                7             6              14            8                1.059417
               12             7              13            8                0.875945
               13             8              15           34                0.921864
               14             8              15           34                0.860069
               15            34              16            8                1.828158
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2947.0141444551
        Total Correlation Energy:                                          -2.5452838142
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1270594308
        Total MDCI Energy:                                              -2949.5594282693
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2946.9703648436 
       SCF energy with basis  cc-pVQZ :  -2947.0141444551 
       CBS SCF Energy                    :  -2947.0273354238 
       Correlation energy with basis  cc-pVTZ :  -2946.9703648436 
       Correlation energy with basis  cc-pVQZ :  -2947.0141444551 
       CBS Correlation Energy            :     -2.6830924567 
       CBS Total Energy                  :  -2949.7104278805 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   112
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             443
     number of aux C basis functions:       1953
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14395-Zkr9/int_phh.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.6297138504
        Electronic Contribution:
                  0    
      0      33.181766
      1      20.958924
      2       0.543365
        Nuclear Contribution:
                  0    
      0     -33.705862
      1     -21.583527
      2       1.085380
        Total Dipole moment:
                  0    
      0      -0.524095
      1      -0.624604
      2       1.628745
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.063975000000    0.088079000000   -0.081851000000
               1 C      1.051917000000    1.051008000000   -0.220896000000
               2 C      0.368380000000   -1.162071000000    0.457684000000
               3 C      2.365012000000    0.763462000000    0.177639000000
               4 C      2.670681000000   -0.489649000000    0.719497000000
               5 C      1.670541000000   -1.446019000000    0.857100000000
               6 H      0.816576000000    2.026105000000   -0.634492000000
               7 C      3.412759000000    1.774859000000    0.032393000000
               8 H     -0.951802000000    0.314071000000   -0.392493000000
               9 H     -0.410382000000   -1.911301000000    0.566506000000
              10 H      3.686348000000   -0.714821000000    1.026123000000
              11 H      1.912950000000   -2.417669000000    1.277033000000
              12 N      3.241239000000    2.880297000000   -0.601988000000
              13 O      4.374441000000    3.684851000000   -0.576471000000
              14 O      4.618503000000    1.502767000000    0.613755000000
              15 Se     5.878957000000    2.750204000000    0.068143000000
              16 O      6.604305000000    2.102828000000   -1.216984000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.063975000000    0.088079000000   -0.081851000000
               1 C      1.051917000000    1.051008000000   -0.220896000000
               2 C      0.368380000000   -1.162071000000    0.457684000000
               3 C      2.365012000000    0.763462000000    0.177639000000
               4 C      2.670681000000   -0.489649000000    0.719497000000
               5 C      1.670541000000   -1.446019000000    0.857100000000
               6 H      0.816576000000    2.026105000000   -0.634492000000
               7 C      3.412759000000    1.774859000000    0.032393000000
               8 H     -0.951802000000    0.314071000000   -0.392493000000
               9 H     -0.410382000000   -1.911301000000    0.566506000000
              10 H      3.686348000000   -0.714821000000    1.026123000000
              11 H      1.912950000000   -2.417669000000    1.277033000000
              12 N      3.241239000000    2.880297000000   -0.601988000000
              13 O      4.374441000000    3.684851000000   -0.576471000000
              14 O      4.618503000000    1.502767000000    0.613755000000
              15 Se     5.878957000000    2.750204000000    0.068143000000
              16 O      6.604305000000    2.102828000000   -1.216984000000
