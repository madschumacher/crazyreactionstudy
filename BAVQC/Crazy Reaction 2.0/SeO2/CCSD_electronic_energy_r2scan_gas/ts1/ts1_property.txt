-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -2946.8756457183
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1357     6.0000    -0.1357     3.7896     3.7896    -0.0000
  1   0     6.1427     6.0000    -0.1427     3.8557     3.8557    -0.0000
  2   0     6.1382     6.0000    -0.1382     3.8281     3.8281    -0.0000
  3   0     5.9312     6.0000     0.0688     3.7660     3.7660    -0.0000
  4   0     6.1418     6.0000    -0.1418     3.8396     3.8396    -0.0000
  5   0     6.1354     6.0000    -0.1354     3.7803     3.7803    -0.0000
  6   0     0.8331     1.0000     0.1669     0.9824     0.9824     0.0000
  7   0     5.8430     6.0000     0.1570     3.8851     3.8851    -0.0000
  8   0     0.8372     1.0000     0.1628     0.9753     0.9753     0.0000
  9   0     0.8353     1.0000     0.1647     0.9761     0.9761    -0.0000
 10   0     0.8132     1.0000     0.1868     0.9987     0.9987    -0.0000
 11   0     0.8356     1.0000     0.1644     0.9750     0.9750     0.0000
 12   0     6.7891     7.0000     0.2109     3.7154     3.7154    -0.0000
 13   0     8.4705     8.0000    -0.4705     1.7461     1.7461     0.0000
 14   0     8.6394     8.0000    -0.6394     1.8769     1.8769    -0.0000
 15   0    32.8959    34.0000     1.1041     3.7674     3.7674     0.0000
 16   0     8.5828     8.0000    -0.5828     1.9500     1.9500    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.397148
                0             6               2            6                1.362889
                0             6               8            1                0.973804
                1             6               3            6                1.304159
                1             6               6            1                0.971579
                2             6               5            6                1.371994
                2             6               9            1                0.975177
                3             6               4            6                1.343810
                3             6               7            6                1.072831
                4             6               5            6                1.377727
                4             6              10            1                0.949128
                5             6              11            1                0.974664
                7             6              12            7                2.411348
                7             6              13            8                0.179426
                7             6              14            8                0.156449
               12             7              13            8                1.220568
               13             8              15           34                0.277346
               14             8              15           34                1.613867
               15            34              16            8                1.827574
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.8756457186
        Total Correlation Energy:                                          -2.3778998929
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1219777549
        Total MDCI Energy:                                              -2949.2535456115
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -2946.9195866106
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 2
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 19
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.1014     6.0000    -0.1014     3.7846     3.7846     0.0000
  1   0     6.1381     6.0000    -0.1381     3.8872     3.8872     0.0000
  2   0     6.1416     6.0000    -0.1416     3.8506     3.8506    -0.0000
  3   0     6.0098     6.0000    -0.0098     3.7676     3.7676    -0.0000
  4   0     6.1568     6.0000    -0.1568     3.8356     3.8356    -0.0000
  5   0     6.0929     6.0000    -0.0929     3.7707     3.7707    -0.0000
  6   0     0.8406     1.0000     0.1594     0.9957     0.9957     0.0000
  7   0     5.6929     6.0000     0.3071     3.8217     3.8217    -0.0000
  8   0     0.8446     1.0000     0.1554     0.9859     0.9859     0.0000
  9   0     0.8354     1.0000     0.1646     0.9840     0.9840    -0.0000
 10   0     0.8167     1.0000     0.1833     1.0116     1.0116     0.0000
 11   0     0.8430     1.0000     0.1570     0.9850     0.9850    -0.0000
 12   0     6.8896     7.0000     0.1104     3.6607     3.6607     0.0000
 13   0     8.4612     8.0000    -0.4612     1.7673     1.7673    -0.0000
 14   0     8.6743     8.0000    -0.6743     1.8643     1.8643     0.0000
 15   0    32.8438    34.0000     1.1562     3.8000     3.8000    -0.0000
 16   0     8.6173     8.0000    -0.6173     1.9348     1.9348    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.418505
                0             6               2            6                1.374634
                0             6               8            1                0.985676
                1             6               3            6                1.284417
                1             6               6            1                0.987617
                2             6               5            6                1.384316
                2             6               9            1                0.984604
                3             6               4            6                1.315774
                3             6               7            6                1.153747
                4             6               5            6                1.399114
                4             6              10            1                0.956624
                5             6              11            1                0.985505
                7             6              12            7                2.332201
                7             6              13            8                0.154465
                7             6              14            8                0.160856
               12             7              13            8                1.240251
               13             8              15           34                0.294458
               14             8              15           34                1.625362
               15            34              16            8                1.827632
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                    112
        Number of correlated Electrons                                                72
        Number of alpha correlated Electrons                                          36
        Number of beta correlated Electrons                                           36
        Reference Energy:                                               -2946.9195866059
        Total Correlation Energy:                                          -2.5713026701
        Alpha-Alpha Pairs Correlation Energy (No (T))                       0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                         0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                        0.0000000000
        Singlet pairs energy of double amplitudes (No (T))                  0.0000000000
        Triplet pairs energy of double amplitudes (No (T))                  0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                         -0.1342195330
        Total MDCI Energy:                                              -2949.4908892760
# -----------------------------------------------------------
$ Energy_Extrapolation
   description: The Extrapolation of the energies
   geom. index: 2
   prop. index: 1
       Do EP1                            : 1 
       Do EP2                            : 0 
       Do EP3                            : 0 
       Number of energies                : 2 
       SCF energy with basis  cc-pVTZ :  -2946.8756457186 
       SCF energy with basis  cc-pVQZ :  -2946.9195866059 
       CBS SCF Energy                    :  -2946.9328261676 
       Correlation energy with basis  cc-pVTZ :  -2946.8756457186 
       Correlation energy with basis  cc-pVQZ :  -2946.9195866059 
       CBS Correlation Energy            :     -2.7089840733 
       CBS Total Energy                  :  -2949.6418102409 
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 2
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   112
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             443
     number of aux C basis functions:       1953
     number of aux J basis functions:       0
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           0.000000
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : /scratch/obeletsan/orcajob__14390-XPSh/ts1.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        5.7569364904
        Electronic Contribution:
                  0    
      0      33.796758
      1      19.992890
      2       2.793875
        Nuclear Contribution:
                  0    
      0     -35.738762
      1     -20.713569
      2      -1.877879
        Total Dipole moment:
                  0    
      0      -1.942003
      1      -0.720679
      2       0.915997
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C     -0.086875000000    0.032877000000    0.062698000000
               1 C      0.814276000000    1.054586000000   -0.192496000000
               2 C      0.362657000000   -1.203581000000    0.522579000000
               3 C      2.187499000000    0.825862000000    0.000682000000
               4 C      2.642743000000   -0.416314000000    0.465775000000
               5 C      1.723139000000   -1.421898000000    0.724890000000
               6 H      0.474331000000    2.022847000000   -0.545185000000
               7 C      3.120806000000    1.858500000000   -0.269358000000
               8 H     -1.147653000000    0.204687000000   -0.092269000000
               9 H     -0.350367000000   -1.996817000000    0.725878000000
              10 H      3.705306000000   -0.561448000000    0.623109000000
              11 H      2.072293000000   -2.383925000000    1.086996000000
              12 N      3.494661000000    2.850673000000   -0.803921000000
              13 O      4.380274000000    3.711860000000   -0.916080000000
              14 O      4.912913000000    1.654848000000    1.011046000000
              15 Se     6.100143000000    2.648757000000    0.411885000000
              16 O      6.968254000000    1.915487000000   -0.745532000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C     -0.086875000000    0.032877000000    0.062698000000
               1 C      0.814276000000    1.054586000000   -0.192496000000
               2 C      0.362657000000   -1.203581000000    0.522579000000
               3 C      2.187499000000    0.825862000000    0.000682000000
               4 C      2.642743000000   -0.416314000000    0.465775000000
               5 C      1.723139000000   -1.421898000000    0.724890000000
               6 H      0.474331000000    2.022847000000   -0.545185000000
               7 C      3.120806000000    1.858500000000   -0.269358000000
               8 H     -1.147653000000    0.204687000000   -0.092269000000
               9 H     -0.350367000000   -1.996817000000    0.725878000000
              10 H      3.705306000000   -0.561448000000    0.623109000000
              11 H      2.072293000000   -2.383925000000    1.086996000000
              12 N      3.494661000000    2.850673000000   -0.803921000000
              13 O      4.380274000000    3.711860000000   -0.916080000000
              14 O      4.912913000000    1.654848000000    1.011046000000
              15 Se     6.100143000000    2.648757000000    0.411885000000
              16 O      6.968254000000    1.915487000000   -0.745532000000
