Here, we provide the videos of the reaction.

1. "reaction_irc.mp4" contains visualization of two IRC calculations of the original reaction (PhCNO + SO2 -> PhNCO + SO2) in benzene at r2SCAN-3c level of theory.
2. Folders "cv_def1", "cv_def2", and "cv_def3" contain three sets of results of the metadynamics (MTD) simulations with three definitions of collective variables (CV). 

More details are given in the PDF ESI and in the main text of the manuscript.
