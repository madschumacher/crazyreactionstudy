-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1407.7697942384
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 54.9999752422 
   Number of Beta  Electrons                 54.9999752422 
   Total number of  Electrons               109.9999504844 
   Exchange energy                         -121.9027779145 
   Correlation energy                        -3.8739974021 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7767753166 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697942384 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                970
     Surface Area:         775.6635246253
     Dielectric Energy:     -0.0057183357
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 17
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 18
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.2163     6.0000    -0.2163     3.7549     3.7549     0.0000
  1   0     6.1919     6.0000    -0.1919     3.8404     3.8404     0.0000
  2   0     5.7847     6.0000     0.2153     3.6642     3.6642     0.0000
  3   0     5.9757     6.0000     0.0243     3.6980     3.6980     0.0000
  4   0     6.1861     6.0000    -0.1861     3.7972     3.7972     0.0000
  5   0     6.2203     6.0000    -0.2203     3.7606     3.7606     0.0000
  6   0     0.8143     1.0000     0.1857     0.9556     0.9556    -0.0000
  7   0     5.6157     6.0000     0.3843     3.8285     3.8285    -0.0000
  8   0     0.8190     1.0000     0.1810     0.9602     0.9602     0.0000
  9   0     0.8161     1.0000     0.1839     0.9558     0.9558    -0.0000
 10   0     0.8197     1.0000     0.1803     0.9604     0.9604     0.0000
 11   0     7.2540     7.0000    -0.2540     2.8858     2.8858     0.0000
 12   0     8.1310     8.0000    -0.1310     1.8481     1.8481     0.0000
 13   0     8.2495     8.0000    -0.2495     1.9718     1.9718     0.0000
 14   0    15.3351    16.0000     0.6649     3.3505     3.3505     0.0000
 15   0     8.4279     8.0000    -0.4279     1.7557     1.7557     0.0000
 16   0    17.1426    17.0000    -0.1426     1.1262     1.1262    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.383574
                0             6               2            6                1.335443
                0             6               8            1                0.943923
                1             6               3            6                1.379047
                1             6               6            1                0.957464
                2             6               5            6                1.361121
                2             6              16           17                0.940592
                3             6               4            6                1.369295
                3             6               7            6                0.998116
                4             6               5            6                1.353439
                4             6               9            1                0.962778
                5             6              10            1                0.944627
                7             6              11            7                1.723046
                7             6              13            8                1.058683
               11             7              12            8                0.923110
               12             8              14           16                0.788368
               13             8              14           16                0.781684
               14            16              15            8                1.673777
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0075443046
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       17
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             303
     number of aux C basis functions:       0
     number of aux J basis functions:       458
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -1407.757092
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.5308940256
        Electronic Contribution:
                  0    
      0       0.040688
      1      -0.978867
      2       0.818426
        Nuclear Contribution:
                  0    
      0      -0.266153
      1       0.621735
      2       0.083273
        Total Dipole moment:
                  0    
      0      -0.225464
      1      -0.357132
      2       0.901699
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.072569000000    0.093327000000   -0.096175000000
               1 C      1.061848000000    1.052637000000   -0.230552000000
               2 C      0.399468000000   -1.145778000000    0.450616000000
               3 C      2.374633000000    0.773473000000    0.173453000000
               4 C      2.682459000000   -0.476467000000    0.720122000000
               5 C      1.693596000000   -1.440948000000    0.861387000000
               6 H      0.818442000000    2.023548000000   -0.649165000000
               7 C      3.412728000000    1.786478000000    0.027653000000
               8 H     -0.944735000000    0.303594000000   -0.407877000000
               9 H      3.696184000000   -0.701453000000    1.032875000000
              10 H      1.927893000000   -2.411811000000    1.284039000000
              11 N      3.261349000000    2.897414000000   -0.600763000000
              12 O      4.449225000000    3.644976000000   -0.512454000000
              13 O      4.628681000000    1.534361000000    0.605932000000
              14 S      5.784112000000    2.671701000000    0.015345000000
              15 O      6.421658000000    2.069357000000   -1.160557000000
              16 Cl    -0.851713000000   -2.356854000000    0.626130000000
