-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1285.2470510488
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 62.9999662277 
   Number of Beta  Electrons                 62.9999662277 
   Total number of  Electrons               125.9999324555 
   Exchange energy                         -129.9279425909 
   Correlation energy                        -4.3653797125 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -134.2933223035 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1285.2470510488 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1124
     Surface Area:         849.5831251493
     Dielectric Energy:     -0.0060214362
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 20
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 22
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.2104     6.0000    -0.2104     3.7772     3.7772     0.0000
  1   0     5.8492     6.0000     0.1508     3.5683     3.5683    -0.0000
  2   0     5.8858     6.0000     0.1142     3.6491     3.6491     0.0000
  3   0     6.2250     6.0000    -0.2250     3.7594     3.7594     0.0000
  4   0     6.1757     6.0000    -0.1757     3.8483     3.8483     0.0000
  5   0     0.8156     1.0000     0.1844     0.9561     0.9561     0.0000
  6   0     5.6575     6.0000     0.3425     3.8245     3.8245    -0.0000
  7   0     0.8196     1.0000     0.1804     0.9581     0.9581    -0.0000
  8   0     0.8176     1.0000     0.1824     0.9568     0.9568     0.0000
  9   0     0.8228     1.0000     0.1772     0.9596     0.9596    -0.0000
 10   0     7.2453     7.0000    -0.2453     2.8985     2.8985     0.0000
 11   0     8.1322     8.0000    -0.1322     1.8548     1.8548     0.0000
 12   0     8.2487     8.0000    -0.2487     1.9751     1.9751     0.0000
 13   0    15.3309    16.0000     0.6691     3.3452     3.3452    -0.0000
 14   0     8.4246     8.0000    -0.4246     1.7607     1.7607    -0.0000
 15   0     5.5059     6.0000     0.4941     4.1865     4.1865    -0.0000
 16   0     9.2119     9.0000    -0.2119     1.0133     1.0133    -0.0000
 17   0     9.2083     9.0000    -0.2083     0.9769     0.9769    -0.0000
 18   0     9.2136     9.0000    -0.2136     0.9933     0.9933    -0.0000
 19   0     6.1994     6.0000    -0.1994     3.7804     3.7804    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               2            6                1.362366
                0             6               5            1                0.968840
                0             6              19            6                1.331978
                1             6               2            6                0.101583
                1             6               4            6                1.398920
                1             6              15            6                0.975475
                1             6              19            6                1.340626
                2             6               3            6                1.353789
                2             6               6            6                0.987600
                3             6               4            6                1.337093
                3             6               8            1                0.973637
                4             6               9            1                0.932846
                6             6              10            7                1.712969
                6             6              12            8                1.054237
                7             1              19            6                0.965267
               10             7              11            8                0.930114
               11             8              13           16                0.781330
               12             8              13           16                0.782023
               13            16              14            8                1.677281
               15             6              16            9                1.057632
               15             6              17            9                1.033719
               15             6              18            9                1.037861
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0084414892
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       20
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             367
     number of aux C basis functions:       0
     number of aux J basis functions:       551
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -1285.230322
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.2258086441
        Electronic Contribution:
                  0    
      0      -5.634685
      1      -6.090577
      2       1.599864
        Nuclear Contribution:
                  0    
      0       5.835495
      1       6.202066
      2      -0.754840
        Total Dipole moment:
                  0    
      0       0.200810
      1       0.111488
      2       0.845024
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    20 
    Geometry Index:     1 
    Coordinates:
               0 C      1.059906000000    1.051590000000   -0.240700000000
               1 C      0.379346000000   -1.143172000000    0.466591000000
               2 C      2.367450000000    0.771753000000    0.175048000000
               3 C      2.672831000000   -0.471463000000    0.739734000000
               4 C      1.677837000000   -1.426420000000    0.884411000000
               5 H      0.824055000000    2.019133000000   -0.670645000000
               6 C      3.410514000000    1.782421000000    0.025849000000
               7 H     -0.942451000000    0.310867000000   -0.421127000000
               8 H      3.684526000000   -0.691023000000    1.061878000000
               9 H      1.913417000000   -2.390532000000    1.324150000000
              10 N      3.261898000000    2.887055000000   -0.612717000000
              11 O      4.446794000000    3.632167000000   -0.535747000000
              12 O      4.623465000000    1.533341000000    0.609496000000
              13 S      5.784016000000    2.663963000000    0.014223000000
              14 O      6.427676000000    2.052167000000   -1.152693000000
              15 C     -0.671791000000   -2.209236000000    0.598802000000
              16 F     -1.923460000000   -1.719666000000    0.474731000000
              17 F     -0.524690000000   -3.169212000000   -0.351196000000
              18 F     -0.600642000000   -2.837755000000    1.796582000000
              19 C      0.069904000000    0.092936000000   -0.099239000000
