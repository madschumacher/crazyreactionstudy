-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1285.2467642996
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 62.9999522590 
   Number of Beta  Electrons                 62.9999522590 
   Total number of  Electrons               125.9999045181 
   Exchange energy                         -129.9284179036 
   Correlation energy                        -4.3653648037 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -134.2937827073 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1285.2467642996 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:               1124
     Surface Area:         847.7240988052
     Dielectric Energy:     -0.0061337579
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 20
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 21
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8744     6.0000     0.1256     3.5092     3.5092    -0.0000
  1   0     6.2843     6.0000    -0.2843     3.9912     3.9912     0.0000
  2   0     6.1430     6.0000    -0.1430     3.8859     3.8859    -0.0000
  3   0     5.8997     6.0000     0.1003     3.6514     3.6514     0.0000
  4   0     6.1745     6.0000    -0.1745     3.7752     3.7752     0.0000
  5   0     6.1671     6.0000    -0.1671     3.7248     3.7248    -0.0000
  6   0     0.8039     1.0000     0.1961     0.9507     0.9507     0.0000
  7   0     5.6638     6.0000     0.3362     3.8186     3.8186     0.0000
  8   0     5.5034     6.0000     0.4966     4.1755     4.1755    -0.0000
  9   0     0.8202     1.0000     0.1798     0.9578     0.9578    -0.0000
 10   0     0.8144     1.0000     0.1856     0.9550     0.9550     0.0000
 11   0     0.8314     1.0000     0.1686     0.9633     0.9633    -0.0000
 12   0     7.2478     7.0000    -0.2478     2.8919     2.8919     0.0000
 13   0     8.1317     8.0000    -0.1317     1.8517     1.8517    -0.0000
 14   0     8.2487     8.0000    -0.2487     1.9735     1.9735     0.0000
 15   0    15.3316    16.0000     0.6684     3.3462     3.3462    -0.0000
 16   0     8.4255     8.0000    -0.4255     1.7602     1.7602     0.0000
 17   0     9.2123     9.0000    -0.2123     1.0135     1.0135    -0.0000
 18   0     9.2147     9.0000    -0.2147     0.9931     0.9931     0.0000
 19   0     9.2077     9.0000    -0.2077     0.9748     0.9748     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                1.351075
                0             6               2            6                1.368387
                0             6               8            6                0.969743
                1             6               3            6                1.354259
                1             6               6            1                0.972259
                2             6               5            6                1.394994
                2             6               9            1                0.927101
                3             6               4            6                1.366826
                3             6               7            6                0.977789
                4             6               5            6                1.345018
                4             6              10            1                0.955428
                5             6              11            1                0.959030
                7             6              12            7                1.710751
                7             6              14            8                1.059757
                8             6              17            9                1.056374
                8             6              18            9                1.038643
                8             6              19            9                1.033242
               12             7              13            8                0.925899
               13             8              15           16                0.784140
               14             8              15           16                0.780366
               15            16              16            8                1.676940
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0084765661
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       20
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             367
     number of aux C basis functions:       0
     number of aux J basis functions:       551
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -1285.230053
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.3707954428
        Electronic Contribution:
                  0    
      0      -8.231764
      1       3.881734
      2      -3.247444
        Nuclear Contribution:
                  0    
      0       8.690816
      1      -4.772628
      2       4.644761
        Total Dipole moment:
                  0    
      0       0.459052
      1      -0.890894
      2       1.397317
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    20 
    Geometry Index:     1 
    Coordinates:
               0 C      0.160086148241    0.090500459835    0.019869186870
               1 C      1.142286398597    1.046274558046   -0.184796678248
               2 C      0.455276652024   -1.121051376335    0.649763284059
               3 C      2.448239277143    0.785987627835    0.248368111527
               4 C      2.750719197353   -0.421753220522    0.883249607482
               5 C      1.751385884104   -1.368265454976    1.080329006764
               6 H      0.908319439852    1.988251540763   -0.667944378832
               7 C      3.489761145929    1.787894151921    0.040981239047
               8 C     -1.240138951664    0.330325323547   -0.472589614302
               9 H     -0.325523088337   -1.858796696205    0.806222170921
              10 H      3.763084770989   -0.619945254144    1.218027172318
              11 H      1.987445073537   -2.305336286270    1.574103153565
              12 N      3.344914316656    2.845801290641   -0.672767557898
              13 O      4.527275355147    3.600251560640   -0.630077644033
              14 O      4.696388295707    1.581901750827    0.653081827897
              15 S      5.859576223318    2.678180009528   -0.001189297801
              16 O      6.522232678981    1.992965200467   -1.115576156760
              17 F     -1.494218177910    1.637081752354   -0.693453400110
              18 F     -2.164265166356   -0.117959742459    0.410006840784
              19 F     -1.472016448310   -0.321674557495   -1.641336571249
