#! /bin/bash


python <<-EOF
import molprocesslib as mpl
import numpy as np

Atn,Mass,Trj = mpl.ReadXYZTrj("ts1_r2scan3c_phh_full_trj.xyz")

N = len(Trj)
TRJ = [Trj[N-i-1] for i in range(0,N)]

Atn,Mass,Trj = mpl.ReadXYZTrj("ts2_r2scan3c_phh_full_trj.xyz")

def Swap(xyz,n,m):
    tmp = np.array(xyz[m-1])
    xyz[m-1] = np.array(xyz[n-1])
    xyz[n-1] = np.array(tmp)
    return xyz


for i in range(0,len(Trj)):
    # swap 7 and 11
    Trj[i] = Swap(Trj[i],7,11)
    Trj[i] = Swap(Trj[i],9,12)
    Trj[i] = Swap(Trj[i],2,5)   
    Trj[i] = Swap(Trj[i],1,6)       
    
TRJ = TRJ + [xyz for xyz in Trj]

if True:
    AtnRef,MassRef,TrjRef = mpl.ReadXYZTrj("ref.xyz")
    if not np.array_equal(Atn,AtnRef):
        raise RuntimeError("Reference is different from the Trajectory :(")

    XYZref = TrjRef[0]

outf=open("full.trj", "w")

NTrj = len(TRJ)
Divisor = max(int(0.01*len(TRJ)),1)

CentMode="CoM"

for n,xyz in enumerate(TRJ):
    if n % Divisor == 0:
        print(" %5.0f %% done (%7i out of %7i)" % (1.*n/Divisor, n, NTrj)) 

    newxyz = mpl.OverlayMolecules(xyz,XYZref,Mass,atn=Atn,outf=outf,NumBruteScan=10,refat=None,ReturnNewXYZ=True,CenterMode=CentMode)

outf.close()

EOF
